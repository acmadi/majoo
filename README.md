
## Best Practice Installation
This is a web application built with laravel 8

- [Install Dokcer, Docker Compose](https://laravel.com/docs/routing).
- [Install with Composer ](https://laravel.com/docs/container).
    - composer install 
    - or with : docker run --rm --interactive --tty --volume $PWD:/app composer install --ignore-platform-reqs --no-scripts
    - cp .env.example to .env
- laravel sail run 
    - ./vendor/bin/sail up -d 
- migrate && seed 
    - ./vendor/bin/sail php artisan migrate
    - ./vendor/bin/sail php artisan db:seed 
- installation done
    - open  your browser with address bar : http://localhost
- account  
    - email : admin@gmail.com , password: password, role : admin
    - email : user-1@gmail.com , password: password, role : member

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
