-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               8.0.26 - MySQL Community Server - GPL
-- Server OS:                    Linux
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table post_majoo.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table post_majoo.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.migrations: ~0 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2022_01_31_074809_create_mt_kategori_table', 1),
	(5, '2022_01_31_074809_create_mt_pelanggan_table', 1),
	(6, '2022_01_31_074809_create_mt_produk_table', 1),
	(7, '2022_01_31_074809_create_mt_supplier_table', 1),
	(8, '2022_01_31_074809_create_tr_pembelian_item_table', 1),
	(9, '2022_01_31_074809_create_tr_pembelian_table', 1),
	(10, '2022_01_31_074809_create_tr_penjualan_items_table', 1),
	(11, '2022_01_31_074809_create_tr_penjualan_table', 1),
	(12, '2022_01_31_074810_add_foreign_keys_to_mt_produk_table', 1),
	(13, '2022_01_31_074810_add_foreign_keys_to_tr_pembelian_item_table', 1),
	(14, '2022_01_31_074810_add_foreign_keys_to_tr_pembelian_table', 1),
	(15, '2022_01_31_074810_add_foreign_keys_to_tr_penjualan_items_table', 1),
	(16, '2022_01_31_074810_add_foreign_keys_to_tr_penjualan_table', 1),
	(17, '2022_01_31_080020_add_role_column_to_users_table', 1),
	(18, '2022_02_02_001113_add_gambar_column_to_produk_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table post_majoo.mt_kategori
CREATE TABLE IF NOT EXISTS `mt_kategori` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.mt_kategori: ~6 rows (approximately)
DELETE FROM `mt_kategori`;
/*!40000 ALTER TABLE `mt_kategori` DISABLE KEYS */;
INSERT INTO `mt_kategori` (`id`, `nama`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Kateori 1', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(2, 'Kateori 2', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(3, 'Kateori 3', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(4, 'Kateori 4', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(5, 'Kateori 5', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL);
/*!40000 ALTER TABLE `mt_kategori` ENABLE KEYS */;

-- Dumping structure for table post_majoo.mt_pelanggan
CREATE TABLE IF NOT EXISTS `mt_pelanggan` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama_pelanggan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_toko` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci,
  `telp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.mt_pelanggan: ~5 rows (approximately)
DELETE FROM `mt_pelanggan`;
/*!40000 ALTER TABLE `mt_pelanggan` DISABLE KEYS */;
INSERT INTO `mt_pelanggan` (`id`, `nama_pelanggan`, `nama_toko`, `alamat`, `telp`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Pelanggan 1', 'Toko Pelanggan 1', 'Alamat Pelanggan 1', '1099999999999', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(2, 'Pelanggan 2', 'Toko Pelanggan 2', 'Alamat Pelanggan 2', '2099999999999', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(3, 'Pelanggan 3', 'Toko Pelanggan 3', 'Alamat Pelanggan 3', '3099999999999', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(4, 'Pelanggan 4', 'Toko Pelanggan 4', 'Alamat Pelanggan 4', '4099999999999', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(5, 'Pelanggan 5', 'Toko Pelanggan 5', 'Alamat Pelanggan 5', '5099999999999', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL);
/*!40000 ALTER TABLE `mt_pelanggan` ENABLE KEYS */;

-- Dumping structure for table post_majoo.mt_produk
CREATE TABLE IF NOT EXISTS `mt_produk` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barcode` text COLLATE utf8mb4_unicode_ci,
  `nama_barang` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci,
  `satuan` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `harga_jual` double DEFAULT NULL,
  `stok` double DEFAULT NULL,
  `kategori_id` int unsigned NOT NULL,
  `supplier_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `gambar` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 1` (`id`),
  KEY `FK_mt_produk_mt_kategori` (`kategori_id`),
  KEY `FK_mt_produk_mt_supplier` (`supplier_id`),
  CONSTRAINT `FK_mt_produk_mt_kategori` FOREIGN KEY (`kategori_id`) REFERENCES `mt_kategori` (`id`),
  CONSTRAINT `FK_mt_produk_mt_supplier` FOREIGN KEY (`supplier_id`) REFERENCES `mt_supplier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.mt_produk: ~0 rows (approximately)
DELETE FROM `mt_produk`;
/*!40000 ALTER TABLE `mt_produk` DISABLE KEYS */;
INSERT INTO `mt_produk` (`id`, `kode_barang`, `barcode`, `nama_barang`, `deskripsi`, `satuan`, `harga_beli`, `harga_jual`, `stok`, `kategori_id`, `supplier_id`, `created_at`, `updated_at`, `deleted_at`, `gambar`) VALUES
	(1, 'jj', 'jjj', 'jjj', NULL, 'jjj', 20000, 90, 90, 3, 4, '2022-01-31 13:27:04', '2022-01-31 22:26:54', '2022-01-31 22:26:54', NULL),
	(2, 'B00001', '000001', 'Shampo Sunsilk', 'Deskrips', 'biji', 20000, 23000, 550, 2, 3, '2022-01-31 22:27:52', '2022-02-02 00:24:28', NULL, NULL),
	(3, 'B00002', '000002', 'Mie Sedap', NULL, 'dus', 17000, 40000, 2005, 2, 3, '2022-01-31 22:37:15', '2022-02-01 09:25:30', NULL, NULL),
	(4, 'B00003', '000003', 'Masker anti slip', NULL, 'pack', 20000, 35000, 771, 3, 4, '2022-01-31 22:38:11', '2022-02-01 11:39:28', NULL, NULL),
	(5, 'kode barang ---', '000000', 'Nama barang', 'Deskr', 'kg', 10000, 20000, 20, 2, 2, '2022-02-02 00:16:27', '2022-02-02 00:23:22', NULL, '/storage/gambar_produk/QmsVzT5q4cu7JMnBWN52T09m0JUOcvJHhfcCewfw.png');
/*!40000 ALTER TABLE `mt_produk` ENABLE KEYS */;

-- Dumping structure for table post_majoo.mt_supplier
CREATE TABLE IF NOT EXISTS `mt_supplier` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama_supplier` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci,
  `telp` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.mt_supplier: ~6 rows (approximately)
DELETE FROM `mt_supplier`;
/*!40000 ALTER TABLE `mt_supplier` DISABLE KEYS */;
INSERT INTO `mt_supplier` (`id`, `nama_supplier`, `alamat`, `telp`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Supplier A', 'Alamat Supplier A', '08392478247289 A', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(2, 'Supplier B', 'Alamat Supplier B', '08392478247289 B', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(3, 'Supplier C', 'Alamat Supplier C', '08392478247289 C', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(4, 'Supplier D', 'Alamat Supplier D', '08392478247289 D', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(5, 'Supplier E', 'Alamat Supplier E', '08392478247289 E', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL),
	(6, 'Supplier F', 'Alamat Supplier F', '08392478247289 F', '2022-01-31 13:25:49', '2022-01-31 13:25:49', NULL);
/*!40000 ALTER TABLE `mt_supplier` ENABLE KEYS */;

-- Dumping structure for table post_majoo.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table post_majoo.tr_pembelian
CREATE TABLE IF NOT EXISTS `tr_pembelian` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `no_pembelian` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_pembelian` datetime DEFAULT NULL,
  `supplier_id` int unsigned DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `users_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 1` (`id`),
  KEY `FK_tr_pembelian_mt_supplier` (`supplier_id`),
  KEY `FK_tr_pembelian_users` (`users_id`),
  CONSTRAINT `FK_tr_pembelian_mt_supplier` FOREIGN KEY (`supplier_id`) REFERENCES `mt_supplier` (`id`),
  CONSTRAINT `FK_tr_pembelian_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.tr_pembelian: ~0 rows (approximately)
DELETE FROM `tr_pembelian`;
/*!40000 ALTER TABLE `tr_pembelian` DISABLE KEYS */;
INSERT INTO `tr_pembelian` (`id`, `no_pembelian`, `tgl_pembelian`, `supplier_id`, `keterangan`, `users_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'xxxxx', '2022-02-01 08:06:10', 1, NULL, 1, '2022-02-01 08:08:32', '2022-02-01 08:08:32', NULL),
	(2, 'xxxxx', '2022-02-01 08:09:49', 2, 'Keterangan', 1, '2022-02-01 08:11:55', '2022-02-01 08:11:55', NULL),
	(3, 'xxxxx', '2022-02-01 09:23:54', 3, NULL, 1, '2022-02-01 09:25:30', '2022-02-01 09:25:30', NULL);
/*!40000 ALTER TABLE `tr_pembelian` ENABLE KEYS */;

-- Dumping structure for table post_majoo.tr_pembelian_item
CREATE TABLE IF NOT EXISTS `tr_pembelian_item` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tr_pembelian_id` int unsigned DEFAULT NULL,
  `mt_produk_id` int unsigned DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `jumlah` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 1` (`id`),
  KEY `FK_tr_pembelian_item_tr_pembelian` (`tr_pembelian_id`),
  KEY `FK_tr_pembelian_item_mt_produk` (`mt_produk_id`),
  CONSTRAINT `FK_tr_pembelian_item_mt_produk` FOREIGN KEY (`mt_produk_id`) REFERENCES `mt_produk` (`id`),
  CONSTRAINT `FK_tr_pembelian_item_tr_pembelian` FOREIGN KEY (`tr_pembelian_id`) REFERENCES `tr_pembelian` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.tr_pembelian_item: ~0 rows (approximately)
DELETE FROM `tr_pembelian_item`;
/*!40000 ALTER TABLE `tr_pembelian_item` DISABLE KEYS */;
INSERT INTO `tr_pembelian_item` (`id`, `tr_pembelian_id`, `mt_produk_id`, `harga_beli`, `jumlah`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 2, 2000, 123, '2022-02-01 08:08:32', '2022-02-01 08:08:32', NULL),
	(2, 1, 3, 3000, 2000, '2022-02-01 08:08:32', '2022-02-01 08:08:32', NULL),
	(3, 2, 3, 14000, 1, '2022-02-01 08:11:55', '2022-02-01 08:11:55', NULL),
	(4, 3, 2, 20000, 30, '2022-02-01 09:25:30', '2022-02-01 09:25:30', NULL),
	(5, 3, 3, 17000, 4, '2022-02-01 09:25:30', '2022-02-01 09:25:30', NULL),
	(6, 3, 4, 20000, 300, '2022-02-01 09:25:30', '2022-02-01 09:25:30', NULL);
/*!40000 ALTER TABLE `tr_pembelian_item` ENABLE KEYS */;

-- Dumping structure for table post_majoo.tr_penjualan
CREATE TABLE IF NOT EXISTS `tr_penjualan` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `no_penjualan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_penjualan` datetime DEFAULT NULL,
  `mt_pelanggan_id` int unsigned DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `total_bayar` double DEFAULT NULL,
  `users_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 1` (`id`),
  KEY `FK_tr_penjualan_users` (`users_id`),
  CONSTRAINT `FK_tr_penjualan_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.tr_penjualan: ~0 rows (approximately)
DELETE FROM `tr_penjualan`;
/*!40000 ALTER TABLE `tr_penjualan` DISABLE KEYS */;
INSERT INTO `tr_penjualan` (`id`, `no_penjualan`, `tgl_penjualan`, `mt_pelanggan_id`, `keterangan`, `total_bayar`, `users_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'xxxxxxxxxxxx', '2022-02-01 05:50:29', 1, 'Ketarnagannnn', 1902370, 1, '2022-02-01 05:51:37', '2022-02-01 06:40:30', NULL),
	(2, 'xxxxxxxxxxxx', '2022-02-01 05:56:37', 1, 'Keternagan yaaa', 1902370, 1, '2022-02-01 05:58:37', '2022-02-01 05:59:04', NULL),
	(5, 'xxxxxxxxxxxx', '2022-02-01 06:40:22', 1, NULL, 158726250, 1, '2022-02-01 06:44:49', '2022-02-01 06:45:14', NULL),
	(6, 'xxxxxxxxxxxx', '2022-02-01 06:45:08', 1, NULL, 39600, 1, '2022-02-01 06:50:15', '2022-02-01 06:50:15', NULL),
	(7, 'xxxxxxxxxxxx', '2022-02-01 07:07:49', 1, NULL, 84680, 1, '2022-02-01 07:08:11', '2022-02-01 07:08:11', NULL),
	(8, 'xxxxxxxxxxxx', '2022-02-01 07:09:30', 1, NULL, 22770, 1, '2022-02-01 07:09:51', '2022-02-01 07:09:51', NULL),
	(9, 'xxxxxxxxxxxx', '2022-02-01 11:39:01', 5, NULL, 693000, 1, '2022-02-01 11:39:28', '2022-02-01 11:39:28', NULL);
/*!40000 ALTER TABLE `tr_penjualan` ENABLE KEYS */;

-- Dumping structure for table post_majoo.tr_penjualan_items
CREATE TABLE IF NOT EXISTS `tr_penjualan_items` (
  `id` int unsigned DEFAULT NULL,
  `tr_penjualan_id` int unsigned DEFAULT NULL,
  `mt_produk_id` int unsigned DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `harga_jual` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `jumlah` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  KEY `FK_tr_penjualan_items_tr_penjualan` (`tr_penjualan_id`),
  KEY `FK_tr_penjualan_items_mt_produk` (`mt_produk_id`),
  CONSTRAINT `FK_tr_penjualan_items_mt_produk` FOREIGN KEY (`mt_produk_id`) REFERENCES `mt_produk` (`id`),
  CONSTRAINT `FK_tr_penjualan_items_tr_penjualan` FOREIGN KEY (`tr_penjualan_id`) REFERENCES `tr_penjualan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.tr_penjualan_items: ~0 rows (approximately)
DELETE FROM `tr_penjualan_items`;
/*!40000 ALTER TABLE `tr_penjualan_items` DISABLE KEYS */;
INSERT INTO `tr_penjualan_items` (`id`, `tr_penjualan_id`, `mt_produk_id`, `harga_beli`, `harga_jual`, `diskon`, `jumlah`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 2, 20000, 23000, 9, 89, '2022-02-01 05:51:37', '2022-02-01 05:51:37', NULL),
	(2, 1, 3, 60000, 40000, 1, 1, '2022-02-01 05:51:37', '2022-02-01 05:51:37', NULL),
	(3, 2, 2, 20000, 23000, 9, 89, '2022-02-01 05:58:37', '2022-02-01 05:58:37', NULL),
	(4, 2, 3, 60000, 40000, 1, 1, '2022-02-01 05:58:37', '2022-02-01 05:58:37', NULL),
	(5, 5, 3, 60000, 40000, 1, 4001, '2022-02-01 06:44:49', '2022-02-01 06:44:49', NULL),
	(6, 5, 4, 30000, 35000, 9, 9, '2022-02-01 06:44:49', '2022-02-01 06:44:49', NULL),
	(7, 6, 3, 60000, 40000, 1, 1, '2022-02-01 06:50:15', '2022-02-01 06:50:15', NULL),
	(8, 7, 2, 20000, 23000, 2, 2, '2022-02-01 07:08:11', '2022-02-01 07:08:11', NULL),
	(9, 7, 3, 60000, 40000, 1, 1, '2022-02-01 07:08:11', '2022-02-01 07:08:11', NULL),
	(10, 8, 2, 20000, 23000, 1, 1, '2022-02-01 07:09:51', '2022-02-01 07:09:51', NULL),
	(11, 9, 4, 20000, 35000, 1, 20, '2022-02-01 11:39:28', '2022-02-01 11:39:28', NULL);
/*!40000 ALTER TABLE `tr_penjualan_items` ENABLE KEYS */;

-- Dumping structure for table post_majoo.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table post_majoo.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`) VALUES
	(1, 'admin', 'admin@gmail.com', '2022-01-31 13:25:48', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QMA5hhIhUCVWIc1IcEmXulWJyfqgt2CnyQq79BmkcBVFj6j3e6nVtHMRDS4e', '2022-01-31 13:25:48', '2022-01-31 13:25:48', 1),
	(2, 'user_1', 'user-1@gmail.com', '2022-01-31 13:25:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1SDp845erT', '2022-01-31 13:25:49', '2022-01-31 13:25:49', 2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
