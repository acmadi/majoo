<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
       $users = $this->seedUser();
       $users = $this->seedProdukKategori();
       $users = $this->seedPelanggan();
       $users = $this->seedSupplier();
    }
    public function seedUser()
    {
        $result[] = \App\Models\User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => \Str::random(10),
            'role_id' => 1, // admin
        ]);
        $result[] = \App\Models\User::create([
            'name' => 'user_1',
            'email' => 'user-1@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => \Str::random(10),
            'role_id' => 2, // member
        ]);
        return $result;
    }
    public function seedProdukKategori()
    {
        $kateories = [];
        foreach (range(1,5) as $key => $value) {
            $kateories[]=\App\Models\MtKategori::create([
                'nama'=> 'Kateori '.$value
            ]);
        }
        return $kateories;
    }
    public function seedPelanggan()
    {
        $results = [];
        foreach (range(1,5) as $key => $value) {
            $results[]=\App\Models\MtPelanggan::create([
                'nama_pelanggan'=> 'Pelanggan '.$value,
                'nama_toko'=> 'Toko Pelanggan '.$value,
                'alamat'=> 'Alamat Pelanggan '.$value,
                'telp'=> $value.'099999999999',
            ]);
        }
        return $results;
    }
    public function seedSupplier()
    {
        $results = [];
        foreach (range('A','F') as $key => $value) {
            $results[]=\App\Models\MtSupplier::create([
                'nama_supplier'=>'Supplier '.$value,
                'alamat'=>'Alamat Supplier '.$value,
                'telp'=>'08392478247289 '.$value,
            ]);
        }
        return $results;
    }
}
