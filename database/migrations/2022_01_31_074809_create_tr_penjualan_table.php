<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrPenjualanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_penjualan', function (Blueprint $table) {
            $table->increments('id')->index('Index 1');
            $table->string('no_penjualan', 50)->nullable();
            $table->dateTime('tgl_penjualan')->nullable();
            $table->unsignedInteger('mt_pelanggan_id')->nullable();
            $table->text('keterangan')->nullable();
            $table->double('total_bayar')->nullable();
            $table->unsignedInteger('users_id')->nullable()->index('FK_tr_penjualan_users');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_penjualan');
    }
}
