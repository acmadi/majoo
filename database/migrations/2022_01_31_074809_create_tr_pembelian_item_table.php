<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrPembelianItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_pembelian_item', function (Blueprint $table) {
            $table->increments('id')->index('Index 1');
            $table->unsignedInteger('tr_pembelian_id')->nullable()->index('FK_tr_pembelian_item_tr_pembelian');
            $table->unsignedInteger('mt_produk_id')->nullable()->index('FK_tr_pembelian_item_mt_produk');
            $table->double('harga_beli')->nullable();
            $table->integer('jumlah')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_pembelian_item');
    }
}
