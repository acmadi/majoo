<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMtProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mt_produk', function (Blueprint $table) {
            $table->increments('id')->index('Index 1');
            $table->string('kode_barang', 50)->nullable();
            $table->text('barcode')->nullable();
            $table->string('nama_barang', 50)->nullable();
            $table->longText('deskripsi', 50)->nullable();
            $table->string('satuan', 5)->nullable();
            $table->double('harga_beli')->nullable();
            $table->double('harga_jual')->nullable();
            $table->double('stok')->nullable();
            $table->unsignedInteger('kategori_id')->index('FK_mt_produk_mt_kategori');
            $table->unsignedInteger('supplier_id')->index('FK_mt_produk_mt_supplier');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mt_produk');
    }
}
