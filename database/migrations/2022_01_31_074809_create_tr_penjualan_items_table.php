<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrPenjualanItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_penjualan_items', function (Blueprint $table) {
            $table->increments('id')->index('Index 1');
            $table->unsignedInteger('tr_penjualan_id')->nullable()->index('FK_tr_penjualan_items_tr_penjualan');
            $table->unsignedInteger('mt_produk_id')->nullable()->index('FK_tr_penjualan_items_mt_produk');
            $table->double('harga_beli')->nullable();
            $table->double('harga_jual')->nullable();
            $table->double('diskon')->nullable();
            $table->integer('jumlah')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_penjualan_items');
    }
}
