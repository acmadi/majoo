<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrPembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_pembelian', function (Blueprint $table) {
            $table->increments('id')->index('Index 1');
            $table->string('no_pembelian', 30)->nullable();
            $table->dateTime('tgl_pembelian')->nullable();
            $table->unsignedInteger('supplier_id')->nullable()->index('FK_tr_pembelian_mt_supplier');
            $table->text('keterangan')->nullable();
            $table->unsignedInteger('users_id')->nullable()->index('FK_tr_pembelian_users');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_pembelian');
    }
}
