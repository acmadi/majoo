<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTrPembelianItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tr_pembelian_item', function (Blueprint $table) {
            $table->foreign(['mt_produk_id'], 'FK_tr_pembelian_item_mt_produk')->references(['id'])->on('mt_produk')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['tr_pembelian_id'], 'FK_tr_pembelian_item_tr_pembelian')->references(['id'])->on('tr_pembelian')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tr_pembelian_item', function (Blueprint $table) {
            $table->dropForeign('FK_tr_pembelian_item_mt_produk');
            $table->dropForeign('FK_tr_pembelian_item_tr_pembelian');
        });
    }
}
