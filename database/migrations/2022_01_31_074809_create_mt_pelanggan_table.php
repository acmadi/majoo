<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMtPelangganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mt_pelanggan', function (Blueprint $table) {
            $table->increments('id')->index('Index 1');
            $table->string('nama_pelanggan', 50)->nullable();
            $table->string('nama_toko', 50)->nullable();
            $table->text('alamat')->nullable();
            $table->string('telp', 50)->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mt_pelanggan');
    }
}
