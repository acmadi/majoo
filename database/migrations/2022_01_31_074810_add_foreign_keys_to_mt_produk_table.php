<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMtProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mt_produk', function (Blueprint $table) {
            $table->foreign(['kategori_id'], 'FK_mt_produk_mt_kategori')->references(['id'])->on('mt_kategori')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['supplier_id'], 'FK_mt_produk_mt_supplier')->references(['id'])->on('mt_supplier')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mt_produk', function (Blueprint $table) {
            $table->dropForeign('FK_mt_produk_mt_kategori');
            $table->dropForeign('FK_mt_produk_mt_supplier');
        });
    }
}
