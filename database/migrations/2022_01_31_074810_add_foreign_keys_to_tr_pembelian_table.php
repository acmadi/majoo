<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTrPembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tr_pembelian', function (Blueprint $table) {
            $table->foreign(['supplier_id'], 'FK_tr_pembelian_mt_supplier')->references(['id'])->on('mt_supplier')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['users_id'], 'FK_tr_pembelian_users')->references(['id'])->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tr_pembelian', function (Blueprint $table) {
            $table->dropForeign('FK_tr_pembelian_mt_supplier');
            $table->dropForeign('FK_tr_pembelian_users');
        });
    }
}
