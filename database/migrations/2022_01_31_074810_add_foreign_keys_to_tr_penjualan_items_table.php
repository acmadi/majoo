<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTrPenjualanItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tr_penjualan_items', function (Blueprint $table) {
            $table->foreign(['mt_produk_id'], 'FK_tr_penjualan_items_mt_produk')->references(['id'])->on('mt_produk')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['tr_penjualan_id'], 'FK_tr_penjualan_items_tr_penjualan')->references(['id'])->on('tr_penjualan')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tr_penjualan_items', function (Blueprint $table) {
            $table->dropForeign('FK_tr_penjualan_items_mt_produk');
            $table->dropForeign('FK_tr_penjualan_items_tr_penjualan');
        });
    }
}
