<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMtSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mt_supplier', function (Blueprint $table) {
            $table->increments('id')->index('Index 1');
            $table->string('nama_supplier', 200)->nullable();
            $table->text('alamat')->nullable();
            $table->text('telp')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mt_supplier');
    }
}
