<?php

namespace App\Http\Controllers;

use App\Models\MtKategori;
use App\Models\MtPelanggan;
use Illuminate\Http\Request;
use App\Models\MtProduk;
use App\Models\MtSupplier;
use App\Models\TrPembelian;
use App\Models\TrPenjualan;
use Yajra\DataTables\Facades\DataTables;

class PembelianCartController extends Controller
{
    public function myCarts(Request $request)
    {
        try {
            $carts = collect(session()->has('carts_pembelian') ?session()->get('carts_pembelian'):[]);
            $cartsMapped = $carts->mapWithKeys(function ($item) {
                return [$item['mt_produk_id'] => $item];
            });
            $cartProduks = MtProduk::whereIn('id', $carts->pluck('mt_produk_id'))->get();
            $cartProduks = $cartProduks->map(function ($d) use($cartsMapped)
            {
                $result = $d->toArray();
                $result['jumlah'] = $cartsMapped[$d->id]['jumlah'];
                $result['harga_beli'] = $cartsMapped[$d->id]['harga_beli'];
                $result['mt_produk_id'] = $cartsMapped[$d->id]['mt_produk_id'];
                $result['subtotal'] = $cartsMapped[$d->id]['jumlah']*$cartsMapped[$d->id]['harga_beli'];
                return $result;
            });
            // return $this->resJson('success', 'OK', ['data'=>$carts]);
            return $this->resJson('success', 'OK', ['data'=>$cartProduks->toArray(), 'total'=>$cartProduks->pluck('subtotal')->sum()]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public function checkoutCarts(Request $request)
    {
        try {
            $rules = [
                'tgl_pembelian' => 'required',
                'supplier_id' => 'required|exists:mt_supplier,id',
                // 'keterangan' => 'required',
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());
            if($validator->fails()){
                return $this->resJson('error', 'error', ['carts_pembelian'=>[], 'errors'=>$validator->errors()]);
            }

            $carts = collect(session()->has('carts_pembelian') ?session()->get('carts_pembelian'):[]);
            if($carts->count()==0){
                return $this->resJson('error', 'error', ['carts_pembelian'=>[], 'errors'=>['Tidak keranjang antrian']]);

            }

            $cartProduks = MtProduk::whereIn('id', $carts->pluck('mt_produk_id'))->get();
            if($cartProduks->count()==0){
                return $this->resJson('error', 'error', ['carts_pembelian'=>[], 'errors'=>['Produk tidak ditemukan']]);
            }

            $cartsMapped = $carts->mapWithKeys(function ($item) {
                return [$item['mt_produk_id'] => $item];
            });

            $periksaJumlah = [
                'hasError' => false,
                'produk_out_stock' => [],
            ];
            $cartProduks = $cartProduks->map(function ($d) use($cartsMapped, &$periksaJumlah)
            {
                $result = $d->toArray();
                $result['jumlah'] = $cartsMapped[$d->id]['jumlah'];

                /* if (!$periksaJumlah['hasError']) {
                    // dd($result, $cartsMapped[$d->id]);
                    if ($result['stok'] < $cartsMapped[$d->id]['jumlah']) {
                        $periksaJumlah['hasError'] = true;
                    }
                }
                if ($result['stok'] < $cartsMapped[$d->id]['jumlah']) {
                    $periksaJumlah['produk_out_stock'][] = $result;
                } */
                $result['harga_beli'] = $cartsMapped[$d->id]['harga_beli'];
                $result['mt_produk_id'] = $cartsMapped[$d->id]['mt_produk_id'];
                // $result['subtotal'] = ($cartsMapped[$d->id]['jumlah']*$d->harga_jual)-($cartsMapped[$d->id]['jumlah']*$d->harga_jual*$cartsMapped[$d->id]['harga_beli']/100);
                $result['subtotal'] = ($cartsMapped[$d->id]['jumlah']*$d->harga_beli);
                $result['model'] = $d;
                return $result;
            });
            if ($periksaJumlah['hasError']) {
                return $this->resJson('error', 'Stok Produk tidak mencukupi ', ['produk_out_stock'=>$periksaJumlah['produk_out_stock'], 'errors'=>['Produk tidak tersedia']]);
            }
            $this->trStart();
            $penjualan = TrPembelian::create([
                'no_pembelian'=>\App\Models\TrPembelian::createCode()['new_code'],
                'tgl_pembelian'=>$request->input('tgl_pembelian'),
                'supplier_id'=>$request->input('supplier_id'),
                'keterangan'=>$request->input('keterangan'),
                'users_id' => \Auth::id(),

                // 'no_penjualan'=>'xxxxxxxxxxxx',
                // 'tgl_penjualan' => $request->input('tgl_penjualan'),
                // 'mt_pelanggan_id' => $request->input('mt_pelanggan_id'),
                // 'keterangan' => $request->input('keterangan'),
                // 'total_bayar' => $cartProduks->pluck('subtotal')->sum(),
                // 'users_id' => \Auth::id(),
            ]);
            $pembelianItem = [];
            foreach ($cartProduks as $key => $cart) {
                $pembelianItem[] = $penjualan->pembelianItem()->create([
                    'mt_produk_id' => $cart['mt_produk_id'],
                    // 'harga_beli' => $cart['harga_beli'],
                    // 'harga_jual' => $cart['harga_jual'] ,
                    // 'harga_beli' => $cart['harga_beli'] ,
                    // 'jumlah' => $cart['jumlah'] ,
                    'mt_produk_id',
                    'harga_beli' => $cart['harga_beli'],
                    'jumlah'=>$cart['jumlah'],
                ]);
                $cart['model']->harga_beli = $cart['harga_beli'];
                $cart['model']->stok = $cart['model']->stok + $cart['jumlah'];
                $cart['model']->save();
            }
            $penjualan->setRelation('pembelianItem', $pembelianItem);
            $this->trCommit();
            session()->forget('carts_pembelian');
            // return $this->resJson('success', 'OK', ['data'=>$carts]);
            return $this->resJson('success', 'OK', ['data'=>$penjualan->toArray(), 'total'=>$cartProduks->pluck('subtotal')->sum()]);
        } catch (\Throwable $th) {
            $this->trRollback();
            throw $th;
        }
    }
    public function addCarts(Request $request)
    {
        try {
            $rules = [
                'mt_produk_id' => 'required',
                'jumlah' => 'required|integer|gt:0',
                // 'harga_beli' => 'required',
                'harga_beli' => 'required',
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());
            if($validator->fails()){
                return $this->resJson('error', 'OK', ['carts_pembelian'=>[], 'errors'=>$validator->errors()]);
            }
            // $produk  = MtProduk::find($request->mt_produk_id);
            // if ($produk->stok < $request->jumlah) {
            //     return $this->resJson('error', 'Stok produk tidak cukup', ['carts_pembelian'=>[], 'errors'=>['Stok produk tidak mencukupi, sisa :'.$produk->stok]]);
            // }

            $carts = collect(session()->has('carts_pembelian') ?session()->get('carts_pembelian'): []);
            $carts = $carts->filter(function ($d)use($request)
            {
                return !($d['mt_produk_id']==$request->mt_produk_id);
            });
            $data = [
                'mt_produk_id'=>$request->mt_produk_id,
                'jumlah'=>$request->jumlah,
                'harga_beli'=>$request->harga_beli,
            ];
            $carts->push($data);
            session()->put('carts_pembelian', $carts);

            return $this->resJson('success', 'OK', ['carts_pembelian'=>$carts]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public function editCarts(Request $request, $id)
    {
        try {
            if ($id=='all') {
                session()->forget('carts_pembelian');
            }else{
                if (session()->has('carts_pembelian')) {
                    $carts = session()->get('carts_pembelian')??[];
                    $cart = collect($carts)->filter(function ($d) use($id)
                    {
                        return ($d['mt_produk_id']==$id);
                    });
                    if ($cart->count()>0) {
                        return $this->resJson('success', 'OK', ['cart'=>$cart->values()[0]]);
                    }
                    return $this->resJson('error', 'Data Keranjang tidak ditemukan', ['cart'=>[]]);
                }
            }
            return $this->resJson('error', 'Data Keranjang tidak ditemukan', ['cart'=>[]]);
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }

    }
    public function delCarts(Request $request, $id)
    {
        try {
            if ($id=='all') {
                session()->forget('carts_pembelian');
            }else{
                if (session()->has('carts_pembelian')) {
                    $carts = session()->get('carts_pembelian')??[];
                    $carts = collect($carts)->filter(function ($d) use($id)
                    {
                        return !($d['mt_produk_id']==$id);
                    });
                    session()->put('carts_pembelian', $carts);
                    return $this->resJson('success', 'OK', ['carts_pembelian'=>$carts]);
                }
            }
            return $this->resJson('success', 'OK', ['carts_pembelian'=>[]]);
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }

    }

}
