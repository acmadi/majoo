<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $roles=[
        1=>'Admin',
        2=>'Member',
    ];
    public function index()
    {
        try {
            // $this->authorize('only-admin');
            if (! Gate::allows('only_admin')) {
                abort(403);
            }
            $data['title']    = 'Master Pengguna ';
            $data['side_bar'] = 'master-pengguna';
            return view('admin.pengguna.index', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }

    public function getDataTable(Request $request){
        try {
            $data = User::getDataTable($request);
            return DataTables::eloquent($data)
                    ->addIndexColumn()
                    ->escapeColumns([])
                    ->make();
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            if (! Gate::allows('only_admin')) {
                abort(403);
            }
            $data['title']    = 'pengguna Baru';
            $data['side_bar'] = 'master-pengguna';
            $data['action']   = route('pengguna.store');
            $data['_method']  = 'POST';
            $data['roles']  = $this->roles;
            return view('admin.pengguna.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $rules = [
                'name' =>'required',
                'email' =>'required',
                'password' =>'required',
                'role_id' =>'required',
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            if (! Gate::allows('only_admin')) {
                abort(403);
            }
            $this->trStart();
            $pengguna = new User();

            $param = $request->only(['name', 'email', 'role_id']);
            $param['password'] = Hash::make($request->password);
            // $param['created_by'] = Auth::id();
            $this->saveEloquent($pengguna, $param);

            $this->trCommit();
            return redirect()->route('pengguna.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            if (! Gate::allows('only_admin')) {
                abort(403);
            }
            $data['data']     = User::find($id);
            $data['title']    = 'Edit pengguna '.$data['data']->nama;
            $data['side_bar'] = 'master-agama';
            $data['action'] = route('pengguna.update', $data['data']->id);
            $data['_method'] = 'PUT';
            $data['roles']  = $this->roles;
            return view('admin.pengguna.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $rules = [
                'name' =>'required',
                'email' =>'required',
                'password' =>'required',
                'role_id' =>'required',
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            $this->trStart();

            if (! Gate::allows('only_admin')) {
                abort(403);
            }
            $pengguna = User::find($id);

            $param = $request->only(['name', 'email', 'role_id']);
            $param['password'] = Hash::make($request->password);

            $this->saveEloquent($pengguna, $param);
            $this->trCommit();
            return redirect()->route('pengguna.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if (! Gate::allows('only_admin')) {
                abort(403);
            }
            $this->trStart();
            $User = User::find($id);
            if(empty($User)) return $this->resJson('error', 'Data tidak ditemukan');
            $User->delete();
            $this->trCommit();

            return $this->resJson('success', 'Berhasil Hapus pengguna');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resCatch($th);
        }

        //
    }
}
