<?php

namespace App\Http\Controllers;

use App\Models\MtKategori;
use App\Models\MtPelanggan;
use Illuminate\Http\Request;
use App\Models\MtProduk;
use App\Models\MtSupplier;
use App\Models\TrPeTrPembelian;
use App\Models\TrPembelian;
use Yajra\DataTables\Facades\DataTables;

class ReportPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $tipe_views = [
        '1'=>'Laporan Pembelian',
        '2'=>'Laporan Pembelian Per Produk'
    ];
    public function index()
    {
        try {
            $data['title']    = 'Laporan Transaksi Pembelian';
            $data['side_bar'] = 'master-penjualan';
            $data['pelanggans'] = MtPelanggan::pluck('nama_pelanggan', 'id');
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['tipe_views'] = $this->tipe_views;
            $data['produks'] = MtProduk::pluck('kode_barang', 'id');

            // if (request()->get('penjualan_id')) {
            //     $dataPenjualan = TrPembelian::find(request()->get('penjualan_id'));

            // }
            // $dataPenjualan = $dataPenjualan ?? new TrPembelian();
            // $dataPenjualan->tgl_penjualan = $dataPenjualan->tgl_penjualan??\Carbon\Carbon::now();
            // $data['data'] = $dataPenjualan ;

            return view('admin.report.pembelian.index', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReport(Request $request)
    {
        try {
            $data['title']    = 'penjualan Baru';
            $data['side_bar'] = 'master-penjualan';
            $data['action']   = route('penjualan.store');
            $data['_method']  = 'POST';
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['kategories'] = MtKategori::pluck('nama', 'id');
            $data['params'] = $request->all();
            if ($request->get('tipe_view')==2) {

                return $this->getReportPembelianByProduk($data, $request);
            }
            return $this->getReportPembelian($data, $request);
            // return $this->resJson('success', 'ok', ['html'=>$html]);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }
    public function getReportPembelian($data, $request)
    {
        // mt_produk_id_start
        // item_mt_produk_id_end
        // supplier_id_start
        // supplier_id_end
        // item_diskon
        // date_start
        // date_end
        // tipe_view
        try {
                //code...
            $query = TrPembelian::with(['pembelianItem.produk', 'supplier']);
            if ($request->get('mt_produk_id_start`')) {

            }
            if ($request->get('item_mt_produk_id_end`')) {

            }
            if ($request->get('supplier_id_start`')) {

            }

            if ($request->get('supplier_id_end`')) {

            }

            if ($request->get('item_diskon`')) {

            }

            if ($request->get('date_start`')) {

            }

            if ($request->get('date_end`')) {

            }

            if ($request->get('tipe_view`')) {

            }
            $data['results'] = $query->get();
            // dd($data['results']);
            $html = view('admin.report.pembelian.report', $data)->render();
            return $this->resJson('success', 'ok', ['html'=>$html]);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }
    public function getReportPembelianByProduk($data, $request)
    {
        // mt_produk_id_start
        // item_mt_produk_id_end
        // supplier_id_start
        // supplier_id_end
        // item_diskon
        // date_start
        // date_end
        // tipe_view
        // $query = MtProduk::with(['pembelianItem.penjualan.pelanggan', 'pembelianItem.produk']);
        try {
            $query = MtProduk::with(['pembelianItem.pembelian.supplier']);
            if ($request->get('mt_produk_id_start`')) {

            }
            if ($request->get('item_mt_produk_id_end`')) {

            }
            if ($request->get('supplier_id_start`')) {

            }

            if ($request->get('supplier_id_end`')) {

            }

            if ($request->get('item_diskon`')) {

            }

            if ($request->get('date_start`')) {

            }

            if ($request->get('date_end`')) {

            }

            if ($request->get('tipe_view`')) {

            }
            $data['results'] = $query->get();
            // dd($data['results']);
            $html = view('admin.report.pembelian.report-by-produk', $data)->render();
            return $this->resJson('success', 'ok', ['html'=>$html]);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }

}
