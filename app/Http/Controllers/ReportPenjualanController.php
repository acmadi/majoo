<?php

namespace App\Http\Controllers;

use App\Models\MtKategori;
use App\Models\MtPelanggan;
use Illuminate\Http\Request;
use App\Models\MtProduk;
use App\Models\MtSupplier;
use App\Models\TrPenjualan;
use Yajra\DataTables\Facades\DataTables;

class ReportPenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $tipe_views = [
        '1'=>'Laporan Penjualan',
        '2'=>'Laporan Penjualan Per Produk'
    ];
    public function index()
    {
        try {
            $data['title']    = 'Laporan Transaksi Penjualan';
            $data['side_bar'] = 'master-penjualan';
            $data['pelanggans'] = MtPelanggan::pluck('nama_pelanggan', 'id');
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['tipe_views'] = $this->tipe_views;
            $data['produks'] = MtProduk::pluck('kode_barang', 'id');

            if (request()->get('penjualan_id')) {
                $dataPenjualan = TrPenjualan::find(request()->get('penjualan_id'));

            }
            $dataPenjualan = $dataPenjualan ?? new TrPenjualan();
            $dataPenjualan->tgl_penjualan = $dataPenjualan->tgl_penjualan??\Carbon\Carbon::now();
            $data['data'] = $dataPenjualan ;

            return view('admin.report.penjualan.index', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }

    // public function getDataTable(Request $request){
    //     try {
    //         $data = TrPenjualan::getDataTable($request);
    //         return DataTables::eloquent($data)
    //                 ->addIndexColumn()
    //                 ->escapeColumns([])
    //                 ->make();
    //     } catch (\Throwable $th) {
    //         return $this->resCatch($th);
    //     }
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReport(Request $request)
    {
        try {
            $data['title']    = 'penjualan Baru';
            $data['side_bar'] = 'master-penjualan';
            $data['action']   = route('penjualan.store');
            $data['_method']  = 'POST';
            $data['pelanggans'] = MtPelanggan::pluck('nama_pelanggan', 'id');
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['kategories'] = MtKategori::pluck('nama', 'id');
            $data['params'] = $request->all();
            if ($request->get('tipe_view')==2) {

                return $this->getReportPenjualanByProduk($data, $request);
            }
            return $this->getReportPenjualan($data, $request);
            // return $this->resJson('success', 'ok', ['html'=>$html]);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }
    public function getReportPenjualan($data, $request)
    {
        // mt_produk_id_start
        // item_mt_produk_id_end
        // supplier_id_start
        // supplier_id_end
        // mt_pelanggan_id_start
        // mt_pelanggan_id_end
        // item_diskon
        // date_start
        // date_end
        // tipe_view
        try {
            $query = TrPenjualan::with(['penjualanItem.produk', 'pelanggan']);
            if ($request->get('mt_produk_id_start`')) {

            }
            if ($request->get('item_mt_produk_id_end`')) {

            }
            if ($request->get('mt_pelanggan_id_start`')) {

            }

            if ($request->get('mt_pelanggan_id_end`')) {

            }

            if ($request->get('item_diskon`')) {

            }

            if ($request->get('date_start`')) {

            }

            if ($request->get('date_end`')) {

            }

            if ($request->get('tipe_view`')) {

            }
            $data['results'] = $query->get();
            // dd($data['results']);
            $html = view('admin.report.penjualan.report-penjualan', $data)->render();
            return $this->resJson('success', 'ok', ['html'=>$html]);
        } catch (\Throwable $th) {
            return $this->resViewError($th);

        }

    }
    public function getReportPenjualanByProduk($data, $request)
    {
        // mt_produk_id_start
        // item_mt_produk_id_end
        // supplier_id_start
        // supplier_id_end
        // item_diskon
        // date_start
        // date_end
        // tipe_view
        // $query = MtProduk::with(['penjualanItem.penjualan.pelanggan', 'penjualanItem.produk']);
        try {
            $query = MtProduk::with(['penjualanItem.penjualan']);
            if ($request->get('mt_produk_id_start`')) {

            }
            if ($request->get('item_mt_produk_id_end`')) {

            }
            if ($request->get('supplier_id_start`')) {

            }

            if ($request->get('supplier_id_end`')) {

            }

            if ($request->get('item_diskon`')) {

            }

            if ($request->get('date_start`')) {

            }

            if ($request->get('date_end`')) {

            }

            if ($request->get('tipe_view`')) {

            }
            $data['results'] = $query->get();
            // dd($data['results']);
            $html = view('admin.report.penjualan.report-penjualan-by-produk', $data)->render();
            return $this->resJson('success', 'ok', ['html'=>$html]);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

}
