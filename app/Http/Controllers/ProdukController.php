<?php

namespace App\Http\Controllers;

use App\Models\MtKategori;
use Illuminate\Http\Request;
use App\Models\MtProduk;
use App\Models\MtSupplier;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;

class ProdukController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['title']    = 'Master produk';
            $data['side_bar'] = 'master-produk';
            return view('admin.produk.index', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }

    public function getDataTable(Request $request){
        try {
            $data = MtProduk::getDataTable($request);
            return DataTables::eloquent($data)
                    ->addIndexColumn()
                    ->escapeColumns([])
                    ->make();
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $data['title']    = 'produk Baru';
            $data['side_bar'] = 'master-produk';
            $data['action']   = route('produk.store');
            $data['_method']  = 'POST';
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['kategories'] = MtKategori::pluck('nama', 'id');
            return view('admin.produk.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rules = [
                // 'kode_barang' => 'required' ,
                'barcode' => 'required' ,
                'nama_barang' => 'required|unique:mt_produk' ,
                'satuan' => 'required' ,
                'harga_beli' => 'required' ,
                'harga_jual' => 'required' ,
                'stok' => 'required' ,
                'kategori_id' => 'required' ,
                'supplier_id' => 'required' ,
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            $this->trStart();
            $produk = new MtProduk();
            $param = $request->only([ 'barcode', 'nama_barang', 'satuan', 'harga_beli', 'harga_jual', 'stok', 'kategori_id', 'supplier_id','deskripsi']);
            if ($request->input('gambar')) {
                $param = $request->only([ 'barcode', 'nama_barang', 'satuan', 'harga_beli', 'harga_jual', 'stok', 'kategori_id', 'supplier_id','gambar', 'deskripsi']);
            }
            $param['kode_barang']  = \App\Models\MtProduk::createCode()['new_code'];
            // $param['created_by'] = Auth::id();
            $this->saveEloquent($produk, $param);

            $this->trCommit();
            return redirect()->route('produk.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data['data']     = MtProduk::find($id);
            $data['title']    = 'Edit produk '.$data['data']->nama_barang;
            $data['side_bar'] = 'master-agama';
            $data['action'] = route('produk.update', $data['data']->id);
            $data['_method'] = 'PUT';
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['kategories'] = MtKategori::pluck('nama', 'id');
            return view('admin.produk.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $rules = [
                'kode_barang' => 'required' ,
                'barcode' => 'required' ,
                'nama_barang' => 'required' ,
                'satuan' => 'required' ,
                'harga_beli' => 'required' ,
                'harga_jual' => 'required' ,
                'stok' => 'required' ,
                'kategori_id' => 'required' ,
                'supplier_id' => 'required' ,
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            $this->trStart();

            $produk = MtProduk::find($id);

            $hasOther = MtProduk::where('nama_barang', $request->input('nama_barang'))->first();
            if ($hasOther && $hasOther->id != $id) {
                // return response()->json(['errors'=> ['client_name'=>['field client_name sudah terpakai']]], 400);
                return back()
                ->with('message', 'gagal')
                ->with('errors', ['nama_barang'=>'field nama_barang sudah terpakai'])
                ->with('status','error');
            }
            $param = $request->only(['kode_barang', 'barcode', 'nama_barang', 'satuan', 'harga_beli', 'harga_jual', 'stok', 'kategori_id', 'supplier_id','deskripsi']);
            // $param['updated_by'] = Auth::id();
            if ($request->input('gambar')) {
                $param = $request->only(['kode_barang', 'barcode', 'nama_barang', 'satuan', 'harga_beli', 'harga_jual', 'stok', 'kategori_id', 'supplier_id','gambar','deskripsi']);
            }
            $this->saveEloquent($produk, $param);
            $this->trCommit();
            return redirect()->route('produk.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->trStart();
            $User = MtProduk::find($id);
            if(empty($User)) return $this->resJson('error', 'Data tidak ditemukan');
            $User->delete();
            $this->trCommit();

            return $this->resJson('success', 'Berhasil Hapus produk');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resCatch($th);
        }

        //
    }
    public function unggahGambar(Request $request)
    {
        try {

            $validator = \Validator::make($request->all(),
                [
                    'gambar' => 'mimes:jpg,jpeg,png,bmp,tiff|max:4096',
                ]
                , messageRulesIndo());

            if($validator->fails()){
                return $this->resJson('errors', 'error',['path'=> '', 'preview'=>'', 'errors'=>$validator->errors()->all()] );

            }
            if ($request->hasFile('gambar')) {
                $path = $request->gambar->store('public/gambar_produk');
                return $this->resJson('error', 'Gambar berhasil diunggah',['path'=>Storage::url($path), 'preview'=> asset(Storage::url($path)), 'errors'=>[]] );
            }
            return $this->resJson('error', 'Gagal mengunggah gambar',['path'=> '', 'preview'=>'', 'errors'=>[]] );
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }

        //
    }


}
