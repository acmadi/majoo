<?php

namespace App\Http\Controllers;

use App\Models\MtKategori;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Validator;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['title']    = 'Master Kategori';
            $data['side_bar'] = 'master-kategori';
            return view('admin.kategori.index', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }

    public function getDataTable(Request $request){
        try {
            $data = MtKategori::getDataTable($request);
            return DataTables::eloquent($data)
                    ->addIndexColumn()
                    ->escapeColumns([])
                    ->make();
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $data['title']    = 'Kategori Baru';
            $data['side_bar'] = 'master-Kategori';
            $data['action']   = route('kategori.store');
            $data['_method']  = 'POST';
            return view('admin.kategori.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $rules = [
                'nama'       => 'required',
            ];
            $validator = Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            $this->trStart();
            $agama = new MtKategori();
            $param = $request->except(['_token', 'files']);
            // $param['created_by'] = Auth::id();
            $this->saveEloquent($agama, $param);

            $this->trCommit();
            return redirect()->route('kategori.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data['data']     = MtKategori::find($id);
            $data['title']    = 'Edit Kategori '.$data['data']->nama;
            $data['side_bar'] = 'master-agama';
            $data['action'] = route('kategori.update', $data['data']->id);
            $data['_method'] = 'PUT';
            return view('admin.kategori.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $rules = [
                'nama'       => 'required',
            ];
            $validator = Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            $this->trStart();


            $agama = MtKategori::find($id);
            $param = $request->except(['_token', 'files']);
            // $param['updated_by'] = Auth::id();

            $this->saveEloquent($agama, $param);
            $this->trCommit();
            return redirect()->route('kategori.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->trStart();
            $User = MtKategori::find($id);
            if(empty($User)) return $this->resJson('error', 'Data tidak ditemukan');
            $User->delete();
            $this->trCommit();

            return $this->resJson('success', 'Berhasil Hapus Kategori');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resCatch($th);
        }

        //
    }
}
