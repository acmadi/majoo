<?php

namespace App\Http\Controllers;

use App\Models\MtKategori;
use App\Models\MtPelanggan;
use Illuminate\Http\Request;
use App\Models\MtProduk;
use App\Models\MtSupplier;
use App\Models\TrPembelian;
use Yajra\DataTables\Facades\DataTables;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['title']    = 'Transaksi pembelian';
            $data['side_bar'] = 'master-pembelian';
            $data['pelanggans'] = MtPelanggan::pluck('nama_pelanggan', 'id');
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['produks'] = MtProduk::pluck('kode_barang', 'id');

            if (request()->get('pembelian_id')) {
                $datapembelian = TrPembelian::find(request()->get('pembelian_id'));

            }
            $datapembelian = $datapembelian ?? new TrPembelian();
            $datapembelian->tgl_pembelian = $datapembelian->tgl_pembelian??\Carbon\Carbon::now();
            $data['data'] = $datapembelian ;

            return view('admin.pembelian.index', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }

    public function getDataTable(Request $request){
        try {
            $data = TrPembelian::getDataTable($request);
            return DataTables::eloquent($data)
                    ->addIndexColumn()
                    ->escapeColumns([])
                    ->make();
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $data['title']    = 'pembelian Baru';
            $data['side_bar'] = 'master-pembelian';
            $data['action']   = route('pembelian.store');
            $data['_method']  = 'POST';
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['kategories'] = MtKategori::pluck('nama', 'id');
            return view('admin.pembelian.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'mt_pelanggan_id' => 'required',
            'keterangan' => 'required',
            'total_bayar' => 'required',
        ];
        $validator = \Validator::make($request->all(), $rules, messageRulesIndo());

        if($validator->fails()){
            return back()
            ->with('message', 'gagal')
            ->with('errors', $validator->errors()->all())
            ->with('status','error');
        }
        $this->trStart();
        try {
            $pembelian = new TrPembelian();
            $param = $request->only(['kode_barang', 'barcode', 'nama_barang', 'satuan', 'harga_beli', 'harga_jual', 'stok', 'kategori_id', 'supplier_id',]);
            $param['users_id'] = \Auth::id();
            $param['no_pembelian'] = 'auto';
            $param['tgl_pembelian'] = \Carbon\Carbon::now();
            $this->saveEloquent($pembelian, $param);

            $this->trCommit();
            return redirect()->route('pembelian.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data['data']     = TrPembelian::find($id);
            $data['title']    = 'Edit pembelian '.$data['data']->nama_barang;
            $data['side_bar'] = 'master-agama';
            $data['action'] = route('pembelian.update', $data['data']->id);
            $data['_method'] = 'PUT';
            $data['suppliers'] = MtSupplier::pluck('nama_supplier', 'id');
            $data['kategories'] = MtKategori::pluck('nama', 'id');
            return view('admin.pembelian.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $rules = [
                'kode_barang' => 'required' ,
                'barcode' => 'required' ,
                'nama_barang' => 'required' ,
                'satuan' => 'required' ,
                'harga_beli' => 'required' ,
                'harga_jual' => 'required' ,
                'stok' => 'required' ,
                'kategori_id' => 'required' ,
                'supplier_id' => 'required' ,
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            $this->trStart();

            $pembelian = TrPembelian::find($id);

            $param = $request->only(['kode_barang', 'barcode', 'nama_barang', 'satuan', 'harga_beli', 'harga_jual', 'stok', 'kategori_id', 'supplier_id',]);
            // $param['updated_by'] = Auth::id();

            $this->saveEloquent($pembelian, $param);
            $this->trCommit();
            return redirect()->route('pembelian.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->trStart();
        try {
            $User = TrPembelian::find($id);
            if(empty($User)) return $this->resJson('error', 'Data tidak ditemukan');
            $User->delete();
            $this->trCommit();

            return $this->resJson('success', 'Berhasil Hapus pembelian');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resCatch($th);
        }

        //
    }


}
