<?php

namespace App\Http\Controllers;

use App\Models\MtKategori;
use App\Models\MtPelanggan;
use Illuminate\Http\Request;
use App\Models\MtProduk;
use App\Models\MtSupplier;
use App\Models\TrPenjualan;
use Yajra\DataTables\Facades\DataTables;

class PenjualanCartController extends Controller
{
    public function myCarts(Request $request)
    {
        try {
            $carts = collect(session()->has('carts') ?session()->get('carts'):[]);
            $cartsMapped = $carts->mapWithKeys(function ($item) {
                return [$item['mt_produk_id'] => $item];
            });
            $cartProduks = MtProduk::whereIn('id', $carts->pluck('mt_produk_id'))->get();
            $cartProduks = $cartProduks->map(function ($d) use($cartsMapped)
            {
                $result = $d->toArray();
                $result['jumlah'] = $cartsMapped[$d->id]['jumlah'];
                $result['diskon'] = $cartsMapped[$d->id]['diskon'];
                $result['mt_produk_id'] = $cartsMapped[$d->id]['mt_produk_id'];
                $result['subtotal'] = ($cartsMapped[$d->id]['jumlah']*$d->harga_jual)-($cartsMapped[$d->id]['jumlah']*$d->harga_jual*$cartsMapped[$d->id]['diskon']/100);
                return $result;
            });
            // return $this->resJson('success', 'OK', ['data'=>$carts]);
            return $this->resJson('success', 'OK', ['data'=>$cartProduks->toArray(), 'total'=>$cartProduks->pluck('subtotal')->sum()]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public function checkoutCarts(Request $request)
    {
        try {
            $rules = [
                'tgl_penjualan' => 'required',
                'mt_pelanggan_id' => 'required|exists:mt_pelanggan,id',
                // 'keterangan' => 'required',
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());
            if($validator->fails()){
                return $this->resJson('error', 'error', ['carts'=>[], 'errors'=>$validator->errors()]);
            }

            $carts = collect(session()->has('carts') ?session()->get('carts'):[]);
            if($carts->count()==0){
                return $this->resJson('error', 'error', ['carts'=>[], 'errors'=>['Tidak keranjang antrian']]);

            }

            $cartProduks = MtProduk::whereIn('id', $carts->pluck('mt_produk_id'))->get();
            if($cartProduks->count()==0){
                return $this->resJson('error', 'error', ['carts'=>[], 'errors'=>['Produk tidak ditemukan']]);
            }

            $cartsMapped = $carts->mapWithKeys(function ($item) {
                return [$item['mt_produk_id'] => $item];
            });

            $periksaJumlah = [
                'hasError' => false,
                'produk_out_stock' => [],
            ];
            $cartProduks = $cartProduks->map(function ($d) use($cartsMapped, &$periksaJumlah)
            {
                $result = $d->toArray();
                $result['jumlah'] = $cartsMapped[$d->id]['jumlah'];

                if (!$periksaJumlah['hasError']) {
                    // dd($result, $cartsMapped[$d->id]);
                    if ($result['stok'] < $cartsMapped[$d->id]['jumlah']) {
                        $periksaJumlah['hasError'] = true;
                    }
                }
                if ($result['stok'] < $cartsMapped[$d->id]['jumlah']) {
                    $periksaJumlah['produk_out_stock'][] = $result;
                }
                $result['diskon'] = $cartsMapped[$d->id]['diskon'];
                $result['mt_produk_id'] = $cartsMapped[$d->id]['mt_produk_id'];
                $result['subtotal'] = ($cartsMapped[$d->id]['jumlah']*$d->harga_jual)-($cartsMapped[$d->id]['jumlah']*$d->harga_jual*$cartsMapped[$d->id]['diskon']/100);
                $result['model'] = $d;
                return $result;
            });
            if ($periksaJumlah['hasError']) {
                return $this->resJson('error', 'Stok Produk tidak mencukupi ', ['produk_out_stock'=>$periksaJumlah['produk_out_stock'], 'errors'=>['Produk tidak tersedia']]);
            }
            $this->trStart();
            $penjualan = TrPenjualan::create([
                'no_penjualan'=>\App\Models\TrPenjualan::createCode()['new_code'],
                'tgl_penjualan' => $request->input('tgl_penjualan'),
                'mt_pelanggan_id' => $request->input('mt_pelanggan_id'),
                'keterangan' => $request->input('keterangan'),
                'total_bayar' => $cartProduks->pluck('subtotal')->sum(),
                'users_id' => \Auth::id(),
            ]);
            $penjualanItem = [];
            foreach ($cartProduks as $key => $cart) {
                $penjualanItem[] = $penjualan->penjualanItem()->create([
                    'mt_produk_id' => $cart['mt_produk_id'],
                    'harga_beli' => $cart['harga_beli'],
                    'harga_jual' => $cart['harga_jual'] ,
                    'diskon' => $cart['diskon'] ,
                    'jumlah' => $cart['jumlah'] ,
                ]);
                $cart['model']->stok = $cart['model']->stok - $cart['jumlah'];
                $cart['model']->save();
            }
            $penjualan->setRelation('penjualanItem', $penjualanItem);
            $this->trCommit();
            session()->forget('carts');
            // return $this->resJson('success', 'OK', ['data'=>$carts]);
            return $this->resJson('success', 'OK', ['data'=>$penjualan->toArray(), 'total'=>$cartProduks->pluck('subtotal')->sum()]);
        } catch (\Throwable $th) {
            $this->trRollback();
            throw $th;
        }
    }
    public function addCarts(Request $request)
    {
        try {
            $rules = [
                'mt_produk_id' => 'required',
                'jumlah' => 'required|integer|gt:0',
                'diskon' => 'required',
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());
            if($validator->fails()){
                return $this->resJson('error', 'OK', ['carts'=>[], 'errors'=>$validator->errors()]);
            }
            $produk  = MtProduk::find($request->mt_produk_id);
            if ($produk->stok < $request->jumlah) {
                return $this->resJson('error', 'Stok produk tidak cukup', ['carts'=>[], 'errors'=>['Stok produk tidak mencukupi, sisa :'.$produk->stok]]);
            }

            $carts = collect(session()->has('carts') ?session()->get('carts'): []);
            $carts = $carts->filter(function ($d)use($request)
            {
                return !($d['mt_produk_id']==$request->mt_produk_id);
            });
            $data = [
                'mt_produk_id'=>$request->mt_produk_id,
                'jumlah'=>$request->jumlah,
                'diskon'=>$request->diskon,
            ];
            $carts->push($data);
            session()->put('carts', $carts);

            return $this->resJson('success', 'OK', ['carts'=>$carts]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public function editCarts(Request $request, $id)
    {
        try {
            if ($id=='all') {
                session()->forget('carts');
            }else{
                if (session()->has('carts')) {
                    $carts = session()->get('carts')??[];
                    $cart = collect($carts)->filter(function ($d) use($id)
                    {
                        return ($d['mt_produk_id']==$id);
                    });
                    if ($cart->count()>0) {
                        return $this->resJson('success', 'OK', ['cart'=>$cart->values()[0]]);
                    }
                    return $this->resJson('error', 'Data Keranjang tidak ditemukan', ['cart'=>[]]);
                }
            }
            return $this->resJson('error', 'Data Keranjang tidak ditemukan', ['cart'=>[]]);
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }

    }
    public function delCarts(Request $request, $id)
    {
        try {
            if ($id=='all') {
                session()->forget('carts');
            }else{
                if (session()->has('carts')) {
                    $carts = session()->get('carts')??[];
                    $carts = collect($carts)->filter(function ($d) use($id)
                    {
                        return !($d['mt_produk_id']==$id);
                    });
                    session()->put('carts', $carts);
                    return $this->resJson('success', 'OK', ['carts'=>$carts]);
                }
            }
            return $this->resJson('success', 'OK', ['carts'=>[]]);
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }

    }

}
