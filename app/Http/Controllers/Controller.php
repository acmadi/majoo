<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function resJson($status, $message = 'Sukses', $result = []){
        if(count($result) > 0){
            $response = $result;
        }
        $response['status']  = $status;
        $response['message'] = $message;

        return response()->json($response, 200);
    }


    public function resCatch( $th, $status = 'error', $message = 'Terjadi Kesalahan Server'){
        $th = $this->customThroable($th);
        $response = [
            'status'  => $status,
            'message' => $message,
            'error'   => $th
        ];

        if(config('app.debug') == false){
            app('App\Http\Controllers\TelegramBot\PostController')->catchError($th);
        }

        return response($response, 500);
    }

    public function resViewError($th){
        if(config('app.debug') == false){
            app('App\Http\Controllers\TelegramBot\PostController')->catchError($th);
            return view('errors.500');
        } else{
            dd($th);
        }
    }

    public function saveEloquent($eloquent, $field){
        foreach ($field as $key => $value) {
            $eloquent->$key = $value;
        }

        $eloquent->save();
        return $eloquent;
    }

    public function customThroable($th){
        return  [
            'message' => $th->getMessage(),
            'traceArray' => $th->getTrace(),
        ];
    }

    public function whereCustom($model, $param, $eager = []){
        $model = $model::with($eager);
        foreach ($param as $key => $value) {
            $model->where($key, $value[0], $value[1]);
        }
        $model = $model->get();
        return $model;
    }

    public function getTahunAjaranAktif(){
        // return MTTahunAjaran::getTahunAjaranAktif();
    }
    public function trStart()
    {
        DB::beginTransaction();
    }
    public function trCommit()
    {
        DB::commit();
    }
    public function trRollback()
    {
        DB::rollback();
    }
}
