<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MtPelanggan;
use Yajra\DataTables\Facades\DataTables;

class PelangganController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['title']    = 'Master pelanggan';
            $data['side_bar'] = 'master-pelanggan';
            return view('admin.pelanggan.index', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }
    }

    public function getDataTable(Request $request){
        try {
            $data = MtPelanggan::getDataTable($request);
            return DataTables::eloquent($data)
                    ->addIndexColumn()
                    ->escapeColumns([])
                    ->make();
        } catch (\Throwable $th) {
            return $this->resCatch($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $data['title']    = 'pelanggan Baru';
            $data['side_bar'] = 'master-pelanggan';
            $data['action']   = route('pelanggan.store');
            $data['_method']  = 'POST';
            return view('admin.pelanggan.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $rules = [
                'nama_pelanggan' => 'required' ,
                'nama_toko' => 'required' ,
                'alamat' => 'required' ,
                'telp' => 'required' ,
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            $this->trStart();
            $pelanggan = new MtPelanggan();
            $param = $request->only(['nama_pelanggan', 'nama_toko', 'alamat', 'telp']);
            // $param['created_by'] = Auth::id();
            $this->saveEloquent($pelanggan, $param);

            $this->trCommit();
            return redirect()->route('pelanggan.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data['data']     = MtPelanggan::find($id);
            $data['title']    = 'Edit pelanggan '.$data['data']->nama;
            $data['side_bar'] = 'master-agama';
            $data['action'] = route('pelanggan.update', $data['data']->id);
            $data['_method'] = 'PUT';
            return view('admin.pelanggan.create', $data);
        } catch (\Throwable $th) {
            return $this->resViewError($th);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $rules = [
                'nama_pelanggan' => 'required' ,
                'nama_toko' => 'required' ,
                'alamat' => 'required' ,
                'telp' => 'required' ,
            ];
            $validator = \Validator::make($request->all(), $rules, messageRulesIndo());

            if($validator->fails()){
                return back()
                ->with('message', 'gagal')
                ->with('errors', $validator->errors()->all())
                ->with('status','error');
            }
            $this->trStart();


            $pelanggan = MtPelanggan::find($id);

            $param = $request->only(['nama_pelanggan', 'nama_toko', 'alamat', 'telp']);
            // $param['updated_by'] = Auth::id();

            $this->saveEloquent($pelanggan, $param);
            $this->trCommit();
            return redirect()->route('pelanggan.index')
                    ->with('message', 'Berhasil')
                    ->with('status', 'success');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resViewError($th);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->trStart();
            $User = MtPelanggan::find($id);
            if(empty($User)) return $this->resJson('error', 'Data tidak ditemukan');
            $User->delete();
            $this->trCommit();

            return $this->resJson('success', 'Berhasil Hapus pelanggan');
        } catch (\Throwable $th) {
            $this->trRollback();
            return $this->resCatch($th);
        }

        //
    }

}
