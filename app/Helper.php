<?php

use App\Models\MTTahunAjaran;
use Carbon\Carbon;

/**
 * change plain number to formatted currency
 *
 * @param $number
 * @param $currency
 */
function isJson($string)
{
    return is_object(json_decode($string));
}

function formatCurrency($number, $rupiah = true)
{
    $rp = "";
    if ($rupiah)
        $rp = "Rp.";

    if ($number - (int)$number > 0) {
        return $rp . number_format($number, 2, ',', '.');
    } else {
        return $rp . number_format($number, 0, ',', '.');
    }
}

function indonesian_date($timestamp = '', $date_format = 'j F Y', $suffix = 'WIB')
{
    if (strpos($timestamp, ':') !== false) {
        $suffix = '';
    } elseif ($date_format != 'l') {
        if ($date_format == null) $date_format = 'j F Y';
    }
    $suffix = '';
    if (trim($timestamp) == '') {
        $timestamp = time();
    } elseif (!ctype_digit($timestamp)) {
        $timestamp = strtotime($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace("/S/", "", $date_format);
    $pattern = array(
        '/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
        '/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
        '/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
        '/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
        '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
        '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
        '/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
        '/November/', '/December/',
    );
    $replace = array(
        'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
        'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
        'Jan ', 'Feb ', 'Mar ', 'Apr ', 'Mei ', 'Jun ', 'Jul ', 'Ags ', 'Sep ', 'Okt ', 'Nov ', 'Des ',
        'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'September',
        'Oktober', 'November', 'Desember',
    );
    $date = date($date_format, $timestamp);
    // dd($pattern, $replace, $date);
    $date = preg_replace($pattern, $replace, $date);
    $date = "{$date} {$suffix}";
    return $date;
}

function hari_jawa($date)
{
    $waktu = date('d-m-Y', strtotime($date));
    $jhari = 0;
    $array = explode("-", $waktu);
    $tgl = $array[0];
    $bln = $array[1];
    $thn = $array[2];

    $bulan = "Januari";
    switch ($bln) {
        case 2: {
                $bulan = "Pebruari";
                $jhari = 31;
                break;
            }
        case 3: {
                $bulan = "Maret";
                $jhari = 59;
                break;
            }
        case 4: {
                $bulan = "April";
                $jhari = 90;
                break;
            }
        case 5: {
                $bulan = "Mei";
                $jhari = 120;
                break;
            }
        case 6: {
                $bulan = "Juni";
                $jhari = 151;
                break;
            }
        case 7: {
                $bulan = "Juli";
                $jhari = 181;
                break;
            }
        case 8: {
                $bulan = "Agustus";
                $jhari = 212;
                break;
            }
        case 9: {
                $bulan = "September";
                $jhari = 243;
                break;
            }
        case 10: {
                $bulan = "Oktober";
                $jhari = 273;
                break;
            }
        case 11: {
                $bulan = "Nopember";
                $jhari = 304;
                break;
            }
        case 12: {
                $bulan = "Desember";
                $jhari = 334;
            }
    }

    $jml_kabisat = 1 + ($thn - ($thn % 4)) / 4;
    if ($thn > 100) $jml_kabisat -= ($thn - ($thn % 100)) / 100;
    if ($thn > 399) $jml_kabisat += ($thn - ($thn % 400)) / 400;
    if (($thn % 4) < 1 && $bln < 3) $jml_kabisat--;

    $jmlhari = $thn * 365 + $jhari + $tgl + $jml_kabisat;

    $urutan_hari = $jmlhari % 7;

    switch ($urutan_hari) {
        case 0:
            $hari = "Jumat";
            break;
        case 1:
            $hari = "Sabtu";
            break;
        case 2:
            $hari = "Minggu";
            break;
        case 3:
            $hari = "Senin";
            break;
        case 4:
            $hari = "Selasa";
            break;
        case 5:
            $hari = "Rabu";
            break;
        case 6:
            $hari = "Kamis";
    }

    $pasaran_jawa = $jmlhari % 5;
    switch ($pasaran_jawa) {
        case 0:
            $hari_jawa = "Kliwon";
            break;
        case 1:
            $hari_jawa = "Legi";
            break;
        case 2:
            $hari_jawa = "Pahing";
            break;
        case 3:
            $hari_jawa = "Pon";
            break;
        case 4:
            $hari_jawa = "Wage";
    }

    $hasil = $hari . " " . $hari_jawa;

    //blokir jika terjadi error saat eksekusi
    if ($array[2] > 5879610) $hasil = false; //terjadi error saat dijalankan di laptop pentium IV 1,7MHz dengan RAM 256MB
    if ($tgl > 28) {
        if ((($thn % 4) > 0 && $bln == 2) || $tgl > 30) {
            if ($bln != 1 || $bln != 3 || $bln != 5 || $bln != 7 || $bln != 8 || $bln != 10 || $bln != 12) {
                $hasil = false;
            }
        }
    }
    return $hasil;
}

function phparray_to_mysql($array)
{
    $res = "(";
    foreach ($array as $count => $a) {
        $res .= $a;
        if (isset($array[$count + 1]))
            $res .= ", ";
    }
    return $res . ")";
}


//default time zone

//fungsi check tanggal merah
function tanggalMerah($value)
{
    // dd($value);
    date_default_timezone_set("Asia/Jakarta");
    $array = json_decode(file_get_contents("https://raw.githubusercontent.com/guangrei/Json-Indonesia-holidays/master/calendar.json"), true);
    //check tanggal merah berdasarkan libur nasional
    if (isset($array[$value])) {
        $message = "Tanggal merah " . $array[$value]["deskripsi"];
        $status = true;
    } elseif (date("D", strtotime($value)) === "Sun") {
        //check Tanggal merah berdasarkan hari minggu
        $message = "Tanggal merah hari minggu";
        $status = true;
    } else {
        //bukan Tanggal merah
        $message = "bukan Tanggal merah";
        $status = false;
    }

    $response = [
        'status'  => $status,
        'message' => $message
    ];
    return $response;
}

function remove_html_tags($string, $html_tags)
{
    $tagStr = "";

    foreach ($html_tags as $key => $value) {
        $tagStr .= $key == count($html_tags) - 1 ? $value : "{$value}|";
    }

    $pat_str = array("/(<\s*\b({$tagStr})\b[^>]*>)/i", "/(<\/\s*\b({$tagStr})\b\s*>)/i");
    $result = preg_replace($pat_str, "", $string);
    return $result;
}

function slugCustom($name)
{
    $slug = preg_replace('~[^\pL\d]+~u', '-', $name);
    $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug); // transliterate
    $slug = preg_replace('~[^-\w]+~', '', $slug); // remove unwanted characters
    $slug = trim($slug, '-'); // trim
    $slug = preg_replace('~-+~', '-', $slug); // remove duplicate -
    $slug = strtolower($slug); // lowercase

    return $slug;
}

function cutText($string, $wordsreturned)
{
  $retval = $string;
  $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
  $string = str_replace("\n", " ", $string);
  $array = explode(" ", $string);
  if (count($array)<=$wordsreturned)
  {
    $retval = $string;
  }
  else
  {
    array_splice($array, $wordsreturned);
    $retval = implode(" ", $array)." ...";
  }
  return $retval;
}

function parseError()
{
    $html = '';
    if (session('errors')) {
        $html .= '
            <div class="errors">
                <div class="alert alert-soft-primary alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <div class="d-flex flex-wrap align-items-start">
                        <div class="mr-8pt">
                            <i class="material-icons">access_time</i>
                        </div>
                        <div class="flex" style="min-width: 180px">
                            <small class="text-black-100">
                                <strong>'.session('message').'</strong>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        ';
        foreach (session('errors') as $error) {
            $html .= '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Error</strong> ' . $error . '
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>';
        }
    }
    return $html;
}

function parseErrorJS(){
    $html = '';
    if (session('errors')) {
        foreach (session('errors') as $error) {
            $html .= '<b>'.$error.'</b><br>';
        }
    }
    return $html;
}

function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}

if (!function_exists('getStringWithLength')) {

    function getStringWithLength($string_data = '', $max_length = 20)
    {
        return substr($string_data, 0, $max_length);
    }

}


function getRouteParam($param_route){
    return request()->route($param_route);
}

function formatDate($tanggal, $format){
    return Carbon::parse($tanggal)->format($format);
}

function spellNumber($num ,$dec=4)
{
    $stext = array(
        "Nol",
        "Satu",
        "Dua",
        "Tiga",
        "Empat",
        "Lima",
        "Enam",
        "Tujuh",
        "Delapan",
        "Sembilan",
        "Sepuluh",
        "Sebelas"
    );
    $say  = array(
        "Ribu",
        "Juta",
        "Milyar",
        "Triliun",
        "Biliun"
    );
    $w = "";

    if ($num <0 ) {
        $w  = "Minus ";
        $num *= -1;
    }

    $snum = number_format($num,$dec,",",".");
    $strnum =  explode(".",substr($snum,0,strrpos($snum,",")));
    $koma = substr($snum,strrpos($snum,",")+1);

    $isone = substr($num,0,1)  ==1;
    if (count($strnum)==1) {
        $num = $strnum[0];
        switch (strlen($num)) {
            case 1:
            case 2:
            if (!isset($stext[$strnum[0]])){
                if($num<=19){
                    $w .=$stext[substr($num,1)]." Belas";
                }else{
                    $w .= $stext[substr($num,0,1)]." Puluh ".
                    (intval(substr($num,1))==0 ? "" : $stext[substr($num,1)]);
                }
            }else{
                $w .= $stext[$strnum[0]];
            }
            break;
            case 3:
            $w .=  ($isone ? "Seratus" : spellNumber(substr($num,0,1)) .
                " Ratus").
            " ".(intval(substr($num,1))==0 ? "" : spellNumber(substr($num,1)));
            break;
            case 4:
            $w .=  ($isone ? "Seribu" : spellNumber(substr($num,0,1)) .
                " Ribu").
            " ".(intval(substr($num,1))==0 ? "" : spellNumber(substr($num,1)));
            break;
            default:
            break;
        }
    }else{
        $text = $say[count($strnum)-2];
        $w = ($isone && strlen($strnum[0])==1 && count($strnum) <=2? "Se".strtolower($text) : spellNumber($strnum[0]).' '.$text);
        array_shift($strnum);
        $i =count($strnum)-2;
        foreach ($strnum as $k=>$v) {
            if (intval($v)) {
                $w.= ' '.spellNumber($v).' '.($i >=0 ? $say[$i] : "");
            }
            $i--;
        }
    }
    $w = trim($w);
    if ($dec = intval($koma)) {
        $w .= " Koma ". spellNumber($koma);
    }


    return trim($w);
}
function messageRulesIndo(){
    return [
        'required' => ':attribute harus diisi',
        'unique'   => ':attribute sudah digunakan',
        'min'      => [
                        'numeric' => ':attribute minimal :min.',
                        'file'    => ':attribute minimal :min kb.',
                        'string'  => ':attribute minimal :min karakter.',
                        'array'   => ':attribute minimal :min items.',
                    ],
        'max'      => [
                        'numeric' => ':attribute maksimal :min.',
                        'file'    => ':attribute maksimal :min kb.',
                        'string'  => ':attribute maksimal :min karakter.',
                        'array'   => ':attribute maksimal :min items.',
                    ],
        'string'    => ':attribute harus berupa string.',
        'email'     => ':attribute harus berupa Email .',
        'exists'    => ':attribute tidak ditemukan di database.',
        'in'        => ':attribute silahkan diisi sesuai dengan pilihan',
        'after_or_equal' => ':attribute harus lebih atau sama dengan :date',
        'image'     => ':attribute harus berupa gambar',
        'mimes' => 'hanya Gambar ber ekstensi : jpeg, png, bmp,tiff yang diijinkan.'
        ];
}
