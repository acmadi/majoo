<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MtProduk extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'mt_produk';
    protected $fillable = [
        'kode_barang',
        'barcode',
        'nama_barang',
        'deskripsi',
        'satuan',
        'harga_beli',
        'harga_jual',
        'stok',
        'kategori_id',
        'supplier_id',
    ];
    public static function getDataTable($request){
        $table = self::selectRaw('mt_produk.*');
        $table = $table->with(['kategori', 'supplier']);

        if(!empty($request->nama)){
            $table = $table->where(function($sub) use ($request){
                $sub->where('nama_barang', 'like', '%'.$request->nama.'%');
            });
        }

        return $table;
    }
    public function kategori()
    {
        return $this->belongsTo(MtKategori::class, 'kategori_id');
    }
    public function supplier()
    {
        return $this->belongsTo(MtSupplier::class, 'supplier_id');
    }
    public function penjualanItem()
    {
        return $this->hasMany(TrPenjualanItems::class, 'mt_produk_id');
    }
    public function pembelianItem()
    {
        return $this->hasMany(TrPembelianItem::class, 'mt_produk_id');
    }
    public static function createCode()
    {
        $last_code = self::selectRaw('MAX(CAST(SUBSTRING(kode_barang FROM 6) AS UNSIGNED)) as last_code')->first();
        $urutan = $last_code->last_code;
        $next_sequence = $urutan + 1;

        $prefix = "BRG";
        $new_code = $prefix . sprintf("%03s", $next_sequence);
        return [
            'last_code'=>$last_code,
            'urutan'=>$urutan,
            'next_sequence'=>$next_sequence,
            'new_code'=>$new_code,
        ];
    }

}
