<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrPembelian extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'tr_pembelian';
    protected $fillable = [
        'no_pembelian',
        'tgl_pembelian',
        'supplier_id',
        'keterangan',
        'users_id',
    ];

    public static function getDataTable($request){
        $table = self::selectRaw('tr_pembelian.*');
        $table = $table->with('supplier');
        if(!empty($request->nama)){
            $table = $table->where(function($sub) use ($request){
                $sub->where('no_pembelian', 'like', '%'.$request->nama.'%');
            });
        }

        return $table;
    }
    public function supplier()
    {
        return $this->belongsTo(MtSupplier::class, 'supplier_id');
    }

    public function pembelianItem()
    {
        return $this->hasMany(TrPembelianItem::class, 'tr_pembelian_id');
    }
    public static function createCode()
    {
        $last_code = self::selectRaw('MAX(CAST(SUBSTRING(no_pembelian FROM 6) AS UNSIGNED)) as last_code')->first();
        $urutan = $last_code->last_code;
        $next_sequence = $urutan + 1;

        $prefix = "TRB";
        $new_code = $prefix . sprintf("%09s", $next_sequence);
        return [
            'last_code'=>$last_code,
            'urutan'=>$urutan,
            'next_sequence'=>$next_sequence,
            'new_code'=>$new_code,
        ];
    }

}
