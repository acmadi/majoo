<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtSupplier extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'mt_supplier';
    protected $fillable = [
        'nama_supplier',
        'alamat',
        'telp',
    ];

    public static function getDataTable($request){
        $table = self::selectRaw('mt_supplier.*');

        if(!empty($request->nama)){
            $table = $table->where(function($sub) use ($request){
                $sub->where('nama_supplier', 'like', '%'.$request->nama.'%');
            });
        }

        return $table;
    }
}
