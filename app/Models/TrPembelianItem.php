<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrPembelianItem extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'tr_pembelian_item';
    protected $fillable = [
        'tr_pembelian_id',
        'mt_produk_id',
        'harga_beli',
        'jumlah',
    ];

    public function produk()
    {
        return $this->belongsTo(MtProduk::class,'mt_produk_id');
    }
    public function pembelian()
    {
        return $this->belongsTo(TrPembelian::class,'tr_pembelian_id');
    }
}
