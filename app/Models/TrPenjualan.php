<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrPenjualan extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'tr_penjualan';
    protected $fillable = [
        'no_penjualan',
        'tgl_penjualan',
        'mt_pelanggan_id',
        'keterangan',
        'total_bayar',
        'users_id',
    ];

    public static function getDataTable($request){
        $table = self::selectRaw('tr_penjualan.*');
        $table = $table->with('pelanggan');
        if(!empty($request->nama)){
            $table = $table->where(function($sub) use ($request){
                $sub->where('no_penjualan', 'like', '%'.$request->nama.'%');
            });
        }

        return $table;
    }
    public function penjualanItem()
    {
        return $this->hasMany(TrPenjualanItems::class, 'tr_penjualan_id');
    }
    public function pelanggan()
    {
        return $this->belongsTo(MtPelanggan::class, 'mt_pelanggan_id');
    }
    public static function createCode()
    {
        $last_code = self::selectRaw('MAX(CAST(SUBSTRING(no_penjualan FROM 6) AS UNSIGNED)) as last_code')->first();
        $urutan = $last_code->last_code;
        $next_sequence = $urutan + 1;

        $prefix = "TRJ";
        $new_code = $prefix . sprintf("%09s", $next_sequence);
        return [
            'last_code'=>$last_code,
            'urutan'=>$urutan,
            'next_sequence'=>$next_sequence,
            'new_code'=>$new_code,
        ];
    }

}
