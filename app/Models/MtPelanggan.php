<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtPelanggan extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'mt_pelanggan';
    protected $fillable = [
        'nama_pelanggan',
        'nama_toko',
        'alamat',
        'telp',
    ];

    public static function getDataTable($request){
        $table = self::selectRaw('mt_pelanggan.*');
        
        if(!empty($request->nama)){
            $table = $table->where(function($sub) use ($request){
                $sub->where('nama_pelanggan', 'like', '%'.$request->nama.'%');
            });
        }

        return $table;
    }


}
