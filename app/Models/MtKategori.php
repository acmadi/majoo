<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class MtKategori extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'mt_kategori';
    protected $fillable = ['nama'];

    public static function getDataTable($request){
        $table = self::selectRaw('mt_kategori.*');

        if(!empty($request->nama)){
            $table = $table->where(function($sub) use ($request){
                $sub->where('nama', 'like', '%'.$request->nama.'%');
            });
        }

        return $table;
    }
    
}
