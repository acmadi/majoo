<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrPenjualanItems extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'tr_penjualan_items';
    protected $fillable = [
        'tr_penjualan_id',
        'mt_produk_id',
        'harga_beli',
        'harga_jual',
        'diskon',
        'jumlah',
    ];
    public function produk()
    {
        return $this->belongsTo(MtProduk::class,'mt_produk_id');
    }
    public function penjualan()
    {
        return $this->belongsTo(TrPenjualan::class,'tr_penjualan_id');
    }
    
}
