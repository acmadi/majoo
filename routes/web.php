<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    /* dd(\App\Models\MtProduk::createCode(), \App\Models\TrPembelian::createCode(), \App\Models\TrPenjualan::createCode()); */
    return view('welcome');
    return view('layouts.app-lte');
});

Route::get('/dashboard', function () {
    // return view('dashboard');
    // return view('layouts.app-lte');
    return view('admin.dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')
        // ->middleware('checkRoleAuth')
        ->group(function () {
        // Route::resource('kategori', \App\Http\Controllers\KategoriController::class);

        Route::prefix('pengguna')->group(function () {
            Route::get('', '\App\Http\Controllers\UserController@index')
            ->name('pengguna.index')->middleware('can:only_admin')->middleware('can:only_admin');
            Route::get('/create', '\App\Http\Controllers\UserController@create')
            ->name('pengguna.create')->middleware('can:only_admin');
            Route::get('/edit/{id}', '\App\Http\Controllers\UserController@edit')
            ->name('pengguna.edit')->middleware('can:only_admin');

            Route::post('/create', '\App\Http\Controllers\UserController@store')
            ->name('pengguna.store')->middleware('can:only_admin');
            Route::post('/edit/{id}', '\App\Http\Controllers\UserController@update')
            ->name('pengguna.update')->middleware('can:only_admin');
            Route::post('/delete/{id}', '\App\Http\Controllers\UserController@destroy')
            ->name('pengguna.delete')->middleware('can:only_admin');
            Route::get('/data', [ \App\Http\Controllers\UserController::class, 'getDataTable'])->name('pengguna.data');
        });
        Route::prefix('kategori')->group(function () {
            Route::get('', '\App\Http\Controllers\KategoriController@index')
            ->name('kategori.index');
            Route::get('/create', '\App\Http\Controllers\KategoriController@create')
            ->name('kategori.create');
            Route::get('/edit/{id}', '\App\Http\Controllers\KategoriController@edit')
            ->name('kategori.edit');

            Route::post('/create', '\App\Http\Controllers\KategoriController@store')
            ->name('kategori.store');
            Route::post('/edit/{id}', '\App\Http\Controllers\KategoriController@update')
            ->name('kategori.update');
            Route::post('/delete/{id}', '\App\Http\Controllers\KategoriController@destroy')
            ->name('kategori.delete');
            Route::get('/data', [ \App\Http\Controllers\KategoriController::class, 'getDataTable'])->name('kategori.data');
        });
        Route::prefix('supplier')->group(function () {
            Route::get('', '\App\Http\Controllers\SupplierController@index')
            ->name('supplier.index');
            Route::get('/create', '\App\Http\Controllers\SupplierController@create')
            ->name('supplier.create');
            Route::get('/edit/{id}', '\App\Http\Controllers\SupplierController@edit')
            ->name('supplier.edit');

            Route::post('/create', '\App\Http\Controllers\SupplierController@store')
            ->name('supplier.store');
            Route::post('/edit/{id}', '\App\Http\Controllers\SupplierController@update')
            ->name('supplier.update');
            Route::post('/delete/{id}', '\App\Http\Controllers\SupplierController@destroy')
            ->name('supplier.delete');
            Route::get('/data', [ \App\Http\Controllers\SupplierController::class, 'getDataTable'])->name('supplier.data');
        });
        Route::prefix('pelanggan')->group(function () {
            Route::get('', '\App\Http\Controllers\PelangganController@index')
            ->name('pelanggan.index');
            Route::get('/create', '\App\Http\Controllers\PelangganController@create')
            ->name('pelanggan.create');
            Route::get('/edit/{id}', '\App\Http\Controllers\PelangganController@edit')
            ->name('pelanggan.edit');

            Route::post('/create', '\App\Http\Controllers\PelangganController@store')
            ->name('pelanggan.store');
            Route::post('/edit/{id}', '\App\Http\Controllers\PelangganController@update')
            ->name('pelanggan.update');
            Route::post('/delete/{id}', '\App\Http\Controllers\PelangganController@destroy')
            ->name('pelanggan.delete');
            Route::get('/data', [ \App\Http\Controllers\PelangganController::class, 'getDataTable'])->name('pelanggan.data');
        });
        Route::prefix('produk')->group(function () {
            Route::get('', '\App\Http\Controllers\ProdukController@index')
            ->name('produk.index');
            Route::get('/create', '\App\Http\Controllers\ProdukController@create')
            ->name('produk.create');
            Route::get('/edit/{id}', '\App\Http\Controllers\ProdukController@edit')
            ->name('produk.edit');

            Route::post('/unggah-gambar', '\App\Http\Controllers\ProdukController@unggahGambar')
            ->name('produk.unggah-gambar');
            Route::post('/create', '\App\Http\Controllers\ProdukController@store')
            ->name('produk.store');
            Route::post('/edit/{id}', '\App\Http\Controllers\ProdukController@update')
            ->name('produk.update');
            Route::post('/delete/{id}', '\App\Http\Controllers\ProdukController@destroy')
            ->name('produk.delete');
            Route::get('/data', [ \App\Http\Controllers\ProdukController::class, 'getDataTable'])->name('produk.data');
        });
        Route::prefix('penjualan')->group(function () {
            Route::get('', '\App\Http\Controllers\PenjualanController@index')
            ->name('penjualan.index');
            Route::get('/create', '\App\Http\Controllers\PenjualanController@create')
            ->name('penjualan.create');
            Route::get('/edit/{id}', '\App\Http\Controllers\PenjualanController@edit')
            ->name('penjualan.edit');

            Route::post('/create', '\App\Http\Controllers\PenjualanController@store')
            ->name('penjualan.store');
            Route::post('/edit/{id}', '\App\Http\Controllers\PenjualanController@update')
            ->name('penjualan.update');
            Route::post('/delete/{id}', '\App\Http\Controllers\PenjualanController@destroy')
            ->name('penjualan.delete');
            Route::get('/data', [ \App\Http\Controllers\PenjualanController::class, 'getDataTable'])->name('penjualan.data');

            Route::get('/my-carts', [ \App\Http\Controllers\PenjualanCartController::class, 'myCarts'])->name('penjualan.my-carts');
            Route::post('/my-carts', [ \App\Http\Controllers\PenjualanCartController::class, 'addCarts'])->name('penjualan.add-carts');
            // Route::get('/my-carts/add', [ \App\Http\Controllers\PenjualanCartController::class, 'addCarts'])->name('penjualan.add-carts');
            Route::get('/my-carts/edit/{id}', [ \App\Http\Controllers\PenjualanCartController::class, 'editCarts'])->name('penjualan.edit-carts');
            Route::post('/my-carts/delete/{id}', [ \App\Http\Controllers\PenjualanCartController::class, 'delCarts'])->name('penjualan.del-carts');
            // Route::get('/my-carts/del/{id}', [ \App\Http\Controllers\PenjualanCartController::class, 'delCarts'])->name('penjualan.del-carts');
            Route::post('/my-carts/checkout', [ \App\Http\Controllers\PenjualanCartController::class, 'checkoutCarts'])->name('penjualan.checkout-carts');
        });
        Route::prefix('pembelian')->group(function () {
            Route::get('', '\App\Http\Controllers\PembelianController@index')
            ->name('pembelian.index');
            Route::get('/create', '\App\Http\Controllers\PembelianController@create')
            ->name('pembelian.create');
            Route::get('/edit/{id}', '\App\Http\Controllers\PembelianController@edit')
            ->name('pembelian.edit');

            Route::post('/create', '\App\Http\Controllers\PembelianController@store')
            ->name('pembelian.store');
            Route::post('/edit/{id}', '\App\Http\Controllers\PembelianController@update')
            ->name('pembelian.update');
            Route::post('/delete/{id}', '\App\Http\Controllers\PembelianController@destroy')
            ->name('pembelian.delete');
            Route::get('/data', [ \App\Http\Controllers\PembelianController::class, 'getDataTable'])->name('pembelian.data');

            Route::get('/my-carts', [ \App\Http\Controllers\PembelianCartController::class, 'myCarts'])->name('pembelian.my-carts');
            Route::post('/my-carts', [ \App\Http\Controllers\PembelianCartController::class, 'addCarts'])->name('pembelian.add-carts');
            // Route::get('/my-carts/add', [ \App\Http\Controllers\PembelianCartController::class, 'addCarts'])->name('pembelian.add-carts');
            Route::get('/my-carts/edit/{id}', [ \App\Http\Controllers\PembelianCartController::class, 'editCarts'])->name('pembelian.edit-carts');
            Route::post('/my-carts/delete/{id}', [ \App\Http\Controllers\PembelianCartController::class, 'delCarts'])->name('pembelian.del-carts');
            // Route::get('/my-carts/del/{id}', [ \App\Http\Controllers\PembelianCartController::class, 'delCarts'])->name('pembelian.del-carts');
            Route::post('/my-carts/checkout', [ \App\Http\Controllers\PembelianCartController::class, 'checkoutCarts'])->name('pembelian.checkout-carts');
        });

        Route::prefix('report')->group(function () {
            Route::get('/penjualan', '\App\Http\Controllers\ReportPenjualanController@index')
            ->name('report.penjualan.index');
            Route::get('/penjualan/data', [ \App\Http\Controllers\ReportPenjualanController::class, 'getReport'])
            ->name('report.penjualan.data');

            Route::get('/pembelian', '\App\Http\Controllers\ReportPembelianController@index')
            ->name('report.pembelian.index');
            Route::get('/pembelian/data', [ \App\Http\Controllers\ReportPembelianController::class, 'getReport'])
            ->name('report.pembelian.data');
        });

    });
});

require __DIR__.'/auth.php';

