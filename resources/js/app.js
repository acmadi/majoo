require('./bootstrap');

import Alpine from 'alpinejs';
// import Vue from 'vue';
window.Vue = require('vue');

window.Alpine = Alpine;

Alpine.start();


const ComponentsApp = {
    data() {
        return {
        groceryList: [
            { id: 0, text: 'Vegetables' },
            { id: 1, text: 'Cheese' },
            { id: 2, text: 'Whatever else humans are supposed to eat' }
        ]
        }
    }
}

const app = Vue.createApp(ComponentsApp)

app.component('todo-item', {
    props: ['todo'],
    template: `<li>{{ todo.text }}</li>`,
    setup(props){
        const reload = ()=>{
            alert('122')
        }
        return {
            reload
        }
    }
})
app.component('my-button', {
    // props: ['todo'],
    template: `<button @click="reload">Reload</button>`,
    setup(props){
        const reload = ()=>{
            alert('122')
            window.objTable()
        }
        return {
            reload
        }
    }
})

if($('#components-app').length>0){
    app.mount('#components-app')
}