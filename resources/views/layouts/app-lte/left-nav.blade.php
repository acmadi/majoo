<div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="/" class="nav-link">Home</a>
          </li>
          <li class="nav-item">
            <a href="{{route('pembelian.index')}}" class="nav-link">Transaksi Pembelian</a>
          </li>
          <li class="nav-item">
            <a href="{{route('penjualan.index')}}" class="nav-link">Transaksi Penjualan</a>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Laporan</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="{{route('report.pembelian.index')}}" class="dropdown-item">laporan Pembelian </a></li>
              {{-- <li><a href="#" class="dropdown-item">laporan Pembelian Per Produk </a></li> --}}
              <li><a href="{{route('report.penjualan.index')}}" class="dropdown-item">laporan Penjualan</a></li>
              {{-- <li><a href="#" class="dropdown-item">laporan Penjualan Per Produk </a></li> --}}
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Master Data</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="{{route('pengguna.index')}}" class="dropdown-item">Pengguna </a></li>
              <li><a href="{{route('produk.index')}}" class="dropdown-item">Produk</a></li>
              <li><a href="{{route('supplier.index')}}" class="dropdown-item">Supplier</a></li>
              <li><a href="{{route('pelanggan.index')}}" class="dropdown-item">Pelanggan</a></li>
              <li><a href="{{route('kategori.index')}}" class="dropdown-item">Kategori</a></li>

              <li class="dropdown-divider"></li>

              <!-- Level two dropdown-->
              @if (false)
                <li class="dropdown-submenu dropdown-hover">
                    <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Hover for action</a>
                    <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li>
                        <a tabindex="-1" href="#" class="dropdown-item">level 2</a>
                    </li>

                    <!-- Level three dropdown-->
                    <li class="dropdown-submenu">
                        <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">level 2</a>
                        <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                        <li><a href="#" class="dropdown-item">3rd level</a></li>
                        <li><a href="#" class="dropdown-item">3rd level</a></li>
                        </ul>
                    </li>
                    <!-- End Level three -->

                    <li><a href="#" class="dropdown-item">level 2</a></li>
                    <li><a href="#" class="dropdown-item">level 2</a></li>
                    </ul>
                </li>
              @endif
              
              <!-- End Level two -->
            </ul>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        {{-- <form class="form-inline ml-0 ml-md-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form> --}}
      </div>