@extends('layouts.app-lte')
@section('content')
    <div class="container">
        <div class="row pb-4">
            <div class="col">
                <a href="{{url()->current().'/create'}}" type="button" class="float-right btn btn-primary">Buat Baru</a>
            </div>
        </div>
        <div class="card mb-lg-32pt">
            <div class="card-body">

                <div class="row">
                    <div class="col-12 pb-3">
                        <button class="btn btn-info advance-search">Advance Search</button>
                    </div>
                </div>
                <div class="row show-advance-search hide pb-3">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama">
                        </div>
                    </div>
                    <div class="col-12">
                        <button class="btn btn-success pull-right btn-filter">Filter</button>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table mb-0 thead-border-top-0 table-nowrap yajra-dt">
                        <thead>
                            <tr>
                                <th style="width:1%">No</th>
                                <th style="width:1px">Nama</th>
                                <th style="width:1px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
        <!-- /.row -->
    
        <!-- /.row -->
        </div>
@endsection

@section('modal')
@endsection

@push('js')
    {{-- <script src="{{asset('datatables\jquery.dataTables.min.js')}}"></script> --}}
    {{-- <script src="{{asset('datatables\dataTables.bootstrap4.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js\admin\master\agama\index.js?time='.microtime(true))}}"></script> --}}
    <script>
        datatable()
        function datatable(){
            let param = {
                nama: $('input[name="nama"').val(),
            }
            let ajax_dt = {
                //urll : API_URL + '/admin/master/agama/get-datatablexxxxxxxxxxxxx',
                urll : "{{route('kategori.data')}}",
                method : 'get',
                data : param
            }
            let column_dt = [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                { data: 'nama', name: 'nama'},
                { data: 'aksi', orderable: false, searchable: false, className: 'text-center',
                    render: function(data, type, row, meta){
                        data = `
                            <a href="`+ BASE_URL + '/admin/kategori/edit/' + row.id+`" data-toggle="tooltip" title="Edit" class="btn btn-sm btn-primary">
                                <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                            </a>
                            <button type="button" onclick="postDelete(this)" data-id="`+row.id+`" data-toggle="tooltip" title="Hapus" class="btn btn-sm btn-accent">
                                <i class="fa fa-trash"></i>
                            </button>`
                        return data;
                        }

                    },
            ]
            initDataTables('.yajra-dt', ajax_dt, column_dt, false)
        }
        $('.btn-filter').on('click',function(){
            datatable()
        })

        function postDelete(e){
            let data = {
                id : $(e).data('id'),
            }
            let urll = "{{route('kategori.index')}}/delete/"+data.id
            Swal.fire({
            title: 'Apakah anda yakin hapus data ini?',
            text: "Data yang dihapus tidak bisa dikembalikan",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus'
            }).then((result) => {
                if (result.isConfirmed) {
                    let response = customAjax('post', urll, data)
                    response = JSON.parse(response)
                    callSwall(response)
                    datatable()
                }
            })
        }


        $('.advance-search').on('click', function(){

            if($('.show-advance-search').hasClass('hide')){
                $('.show-advance-search').removeClass('hide');
            }else{
                $('.show-advance-search').addClass('hide')
            }

        })

    </script>
@endpush
@push('css')
    <style>
    .hide{
        display:none
    }
    </style>
    {{-- <link rel="stylesheet" href="{{asset('datatables\jquery.dataTables.css')}}"> --}}
    {{-- <link rel="stylesheet" href="{{asset('datatables\dataTables.bootstrap4.min.css')}}"> --}}
@endpush