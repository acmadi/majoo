<h4 style="text-align:center"> PT Sumber Rejeki</h4>  
<h5 style="text-align:center"> Laporan Pembelian Produk </h5>  
<h5 style="text-align:center"> Periode : {{$params['date_start']??'--'}} sd {{$params['date_end']??'--'}} </h5>  
<table class="table table-sm table-bordered">
    <tbody>
    @foreach ($results??[] as $produk)
        @php
        @endphp
        <tr >
            <td >
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col " style="padding-left: 15px">
                    <address>
                        <span>Produk : {{$produk->nama_barang}} ({{$produk->kode_barang}}).</span><br>
                        <span>Stok : {{$produk->stok}} {{$produk->satuan}}</span><br>
                    </address>
                    </div>
                </div>
            </td>
        </tr>
        <tr class="expandable-body">
            <td >
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Supplier</th>
                        <th>No Transaksi</th>
                        <th>Tanggal</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Sub total</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($produk->pembelianItem as $item)
                        
                        <tr aria-expanded="true">
                            <td>{{$loop->iteration}}</td>
                            @php
                            //dd($item);
                            @endphp
                            <td>{{$item->pembelian->supplier->nama_supplier}}</td>
                            <td>{{$item->pembelian->no_penjualan}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->jumlah}}</td>
                            <td>{{formatCurrency($item->harga_beli)}}</td>
                            <td>{{formatCurrency($item->jumlah*$item->harga_beli)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        
    @endforeach
    </tbody>
</table>