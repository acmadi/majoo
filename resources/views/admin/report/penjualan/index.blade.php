@extends('layouts.app-lte')
@section('content')
    <div class="container">
        <div class="card mb-lg-32pt">
            <div class="card-body">
                <div class="row " id="formTransaksiPenualan">
                    <div class="col-12 pb-3">
                        {{-- @include('admin.penjualan.form-penjualan') --}}
                        <div class="row">
                            <!-- /.col-md-6 -->
                            <div class="col-lg-12">
                                <table class="table ">
                                <tbody>
                                    <tr>
                                        <td style="width:15%">Dari Produk</td>
                                        <td>
                                            <select class="form-control form-control-sm js-example-basic-single" style="width: 100%" name="mt_produk_id_start" id="mt_produk_id_start"> 
                                                @foreach ($produks as $key => $name)
                                                <option  value="{{$key}}">{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td style="width:15%">sd Produk</td>
                                        <td>
                                            <select class="form-control form-control-sm js-example-basic-single" style="width: 100%" name="mt_produk_id_end" id="item_mt_produk_id_end"> 
                                                @foreach ($produks as $key => $name)
                                                <option value="{{$key}}">{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%">Dari Pelanggan</td>
                                        <td>
                                            <select class="form-control form-control-sm js-example-basic-single" style="width: 100%" name="mt_pelanggan_id_start" id="mt_pelanggan_id_start"> 
                                                @foreach ($pelanggans as $key => $name)
                                                <option value="{{$key}}">{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td style="width:15%">sd Pelanggan</td>
                                        <td>
                                            <select class="form-control form-control-sm js-example-basic-single" style="width: 100%" name="mt_pelanggan_id_end" id="mt_pelanggan_id_end"> 
                                                @foreach ($pelanggans as $key => $name)
                                                <option value="{{$key}}">{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%">Dari Tgl</td>
                                        <td>
                                            {{-- <input type="number" required name="diskon" id="item_diskon"  class="form-control form-control-sm" value="{{$data->diskon ?? old('diskon')}}"> --}}
                                            <div class="input-group date" id="date_start_picker" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#date_start_picker" id="date_start"/>
                                                <div class="input-group-append" data-target="#date_start_picker" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width:15%">sd Tgl</td>
                                        <td>
                                            <div class="input-group date" id="date_end_picker" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#date_end_picker" id="date_end"/>
                                                <div class="input-group-append" data-target="#date_end_picker" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%">Jenis Tampilan</td>
                                        <td>
                                            <select class="form-control form-control-sm js-example-basic-single" style="width: 100%" name="tipe_view" id="tipe_view"> 
                                                @foreach ($tipe_views as $key => $name)
                                                <option value="{{$key}}">{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td style="width:15%"></td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr >
                                    <td class="bg-red-500 break-all " colspan="4">
                                        <button class="btn btn-info float-right" id="showReport">Tampilkan Laporan</button>
                                    </td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                            <!-- /.col-md-6 -->
                        </div>
                    </div>
                </div> 
                <div class="row "  style="max-height: 30%">
                    <button class="btn btn-info float-right" onclick="cetakElement('reportView')">Cetak Laporan</button>
                    </button>
                    <div class="col-lg-12" id="reportView">
                    @include('admin.report.penjualan.report-penjualan')
                    </div >
                </div> 
            </div>
        </div>
    
        <!-- /.row -->
    
        <!-- /.row -->
        </div>
@endsection

@section('modal')
@endsection

@push('js')
    <script>
        
        let totalBayar = 0;
        $('#showReport').on('click', function(e){
            e.preventDefault();
            let param  = {
                'mt_produk_id_start' : $('#mt_produk_id_start').val(),
                'item_mt_produk_id_end' : $('#item_mt_produk_id_end').val(),
                'mt_pelanggan_id_start' : $('#mt_pelanggan_id_start').val(),
                'mt_pelanggan_id_end' : $('#mt_pelanggan_id_end').val(),
                'item_diskon' : $('#item_diskon').val(),
                'date_start' : $('#date_start').val(),
                'date_end' : $('#date_end').val(),
                'tipe_view' : $('#tipe_view').val(),
            }
            let response = customAjax('get', '{{route('report.penjualan.data')}}', param)
            response = JSON.parse(response)
            console.log(response)
            if(response.status=='success'){
                $('#reportView').html(response.html)
            }
           
        })

        $('#date_start_picker').datetimepicker({
            format: 'L'
        });
        $('#date_end_picker').datetimepicker({
            format: 'L'
        });

        
    </script>
    <script>
        
    </script>
@endpush
@push('css')
    <style>
    .hide{
        display:none
    }
    </style>
@endpush