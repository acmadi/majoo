<h4 style="text-align:center"> PT Sumber Rejeki</h4>  
<h5 style="text-align:center"> Laporan Penjualan </h5>  
<h5 style="text-align:center"> Periode : {{$params['date_start']??'--'}} sd {{$params['date_end']??'--'}} </h5>  
<table class="table table-sm table-bordered">
    {{-- <thead>
    <tr>
        <th>#</th>
        <th>User</th>
        <th>Date</th>
        <th>Status</th>
        <th>Reason</th>
    </tr>
    </thead> --}}
    <tbody>
    {{-- <tr aria-expanded="true">
        <td>183</td>
        <td>John Doe</td>
        <td>11-7-2014</td>
        <td>Approved</td>
        <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
    </tr> --}}
    @foreach ($results??[] as $penjualan)
        @php
        @endphp
        <tr >
            <td >
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col " style="padding-left: 15px">
                    <address>
                        <span>Nama Pelanggan : {{$penjualan->pelanggan->nama_pelanggan}} ({{$penjualan->pelanggan->nama_toko}}).</span><br>
                        <span>Nomor : {{$penjualan->no_penjualan}}</span><br>
                        <span>Tanggal : {{$penjualan->created_at}}</span><br>
                    </address>
                    </div>
                </div>
            </td>
        </tr>
        <tr class="expandable-body">
            <td >
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Barang</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Sub total</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($penjualan->penjualanItem as $item)
                        
                        <tr aria-expanded="true">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->produk->kode_barang}} - {{$item->produk->nama_barang}}</td>
                            <td>{{$item->jumlah}}</td>
                            <td>{{$item->harga_jual}}</td>
                            <td>{{formatCurrency($item->jumlah*$item->harga_jual)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        
    @endforeach
    </tbody>
</table>