@extends('layouts.app-lte')
@section('content')
    <div class="container">
        <div class="card mb-lg-32pt">
            <div class="card-body">
                <div class="col">
                    {!!parseError() !!}
                    <form class="formAdd" method="post" action="{{$action}}">
                        @csrf
                        {{-- <input type="hidden" name="_method" value="{{$_method}}"> --}}
                        <div class="col-lg-3  col-sm-12">
                            
                            <div class="form-group">
                                <label>Nama </label>
                                <input type="text" required name="name" class="form-control" value="{{$data->name ?? old('name')}}">
                            </div>
                            
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" required name="email" class="form-control" value="{{$data->email ?? old('email')}}">
                            </div>
                            
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" required name="password" class="form-control" value="" placeholder="**********" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select class="form-control js-example-basic-single" style="width: 100%" name="role_id">
                                    @foreach ($roles as $key => $name)
                                    <option {{($data->role_id ?? old('role_id'))==$key?'selected':''}} value="{{$key}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary btn-add-banner">Submit</button>
                        </div>
                    </form>
                </div>
            
            </div>
        </div>
    
        <!-- /.row -->
    
        <!-- /.row -->
        </div>
@endsection

@section('modal')
@endsection

@push('js')
    
@endpush
@push('css')
@endpush