@extends('layouts.app-lte')
@section('content')
    <div class="container">
        <div class="card mb-lg-32pt">
            <div class="card-body">
                <div class="col">
                    {!!parseError() !!}
                    <form class="formAdd" method="post" action="{{$action}}">
                        @csrf
                        {{-- <input type="hidden" name="_method" value="{{$_method}}"> --}}
                        <div class="col-lg-3  col-sm-12">
                            <div class="form-group">
                                <label>Kode Barang</label>
                                <input type="text" required name="kode_barang" readonly placeholder="auto generated" class="form-control" value="{{$data->kode_barang ?? old('kode_barang')}}">
                            </div>

                            <div class="form-group">
                                <label>Barcode</label>
                                <input type="text" required name="barcode" class="form-control" value="{{$data->barcode ?? old('barcode')}}">
                            </div>

                            <div class="form-group">
                                <label>Nama Barang</label>
                                <input type="text" required name="nama_barang" class="form-control" value="{{$data->nama_barang ?? old('nama_barang')}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Unggah Gambar</label>
                                <input type="hidden"  name="gambar" id="gambar" value="{{$data->gambar ?? old('gambar')}}"/>
                                <input type="file" class="form-control-file" id="gambar_uploader">
                                <br>
                                <img src="{{asset($data->gambar??'')}}" alt="..." class="img-thumbnail" id="img-thumbnail">
                                <br>

                                <div class="progress" id="progress-indicator" style="display: none;" >
                                    <div class="progress-bar progress-bar-animated progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                                </div>
                                <span id="gambar_uploader_error" class="error invalid-feedback">Gagal mengunggah gambar</span>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                {{-- <input type="text" required name="deskripsi" class="form-control" value="{{$data->deskripsi ?? old('deskripsi')}}"> --}}
                                <textarea class="summernote" required name="deskripsi">
                                    {{$data->deskripsi ?? old('deskripsi')}}
                                </textarea>
                            </div>

                            <div class="form-group">
                                <label>Satuan</label>
                                <input type="text" required name="satuan" class="form-control" value="{{$data->satuan ?? old('satuan')}}">
                            </div>

                            <div class="form-group">
                                <label>Harga Beli</label>
                                <input type="number" required name="harga_beli" class="form-control" value="{{$data->harga_beli ?? old('harga_beli')}}">
                            </div>

                            <div class="form-group">
                                <label>Harga Jual</label>
                                <input type="number" required name="harga_jual" class="form-control" value="{{$data->harga_jual ?? old('harga_jual')}}">
                            </div>

                            <div class="form-group">
                                <label>Stok</label>
                                <input type="number" required name="stok" class="form-control" value="{{$data->stok ?? old('stok')}}">
                            </div>

                            <div class="form-group">
                                <label>Kategori</label>
                                {{-- <input type="text" required name="kategori_id" class="form-control" value="{{$data->kategori_id ?? old('kategori_id')}}"> --}}
                                <select class="form-control js-example-basic-single" style="width: 100%" name="kategori_id">
                                    @foreach ($kategories as $key => $name)
                                    <option {{($data->kategori_id ?? old('kategori_id'))==$key?'selected':''}} value="{{$key}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Supplyer</label>
                                {{-- <input type="text" required name="supplier_id" class="form-control" value="{{$data->supplier_id ?? old('supplier_id')}}"> --}}
                                <select class="form-control js-example-basic-single" style="width: 100%" name="supplier_id">
                                    @foreach ($suppliers as $key => $name)
                                    <option {{($data->supplier_id ?? old('supplier_id'))==$key?'selected':''}} value="{{$key}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary btn-add-banner">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <!-- /.row -->

        <!-- /.row -->
        </div>
@endsection

@section('modal')
@endsection

@push('js')
    <!-- Select2 -->
    <script src="{{asset("plugins/select2/js/select2.full.min.js")}}"></script>
    <script>
        $(document).ready(function() {
            //$('.js-example-basic-single').select2();
            //$('#progress-indicator').hide()
            //$('#progress-indicator').show()
            $('#gambar_uploader').on('change', function(e){
                var fileUpload = $(this).get(0);
                    var files = fileUpload.files;
                    console.log(files)
                    var bid = 0;
                    if (files.length != 0) {
                        var data = new FormData();
                        for (var i = 0; i < files.length ; i++) {
                            data.delete('gambar');
                            data.append('gambar', files[i]);
                        }
                        $.ajax({
                            xhr: function () {
                                $('#progress-indicator').show()
                                var xhr = $.ajaxSettings.xhr();
                                xhr.upload.onprogress = function (e) {
                                    console.log(Math.floor(e.loaded / e.total * 100) + '%');
                                     $("#progress-indicator").css("width", Math.floor(e.loaded / e.total * 100) + '%');
                                };
                                return xhr;
                            },
                            contentType: false,
                            processData: false,
                            type: 'POST',
                            data: data,
                            url: '{{route('produk.unggah-gambar')}}',
                            success: function (response) {
                                console.log('response')
                                console.log(response.path)
                                if(response.status=='errors'){
                                    $('.invalid-feedback').show()
                                    $('.invalid-feedback').text(response.errors.join(', '))
                                    $('#img-thumbnail').hide()
                                }else{
                                    $('#gambar').val(response.path)
                                    $('#img-thumbnail').show()
                                    $('#img-thumbnail').attr('src', response.preview)
                                    $('.invalid-feedback').hide()
                                    $('.invalid-feedback').text('')
                                }
                                    $('#progress-indicator').hide()
                            },
                            error: function (response) {
                                console.log('response')
                                console.log(response)
                                //location.href = 'xxx/Index/';
                                $('#progress-indicator').hide()
                                response.responseJSON.message
                                if(response.responseJSON.message.length>0){
                                    $('.invalid-feedback').text(response.responseJSON.message)
                                }
                                $('.invalid-feedback').show()
                            }
                        });
                    }
            })
        });
    </script>
@endpush
@push('css')
  <!-- Select2 -->
  <!-- Select2 -->
<link rel="stylesheet" href="{{asset("plugins/select2/css/select2.min.css")}}">
<link rel="stylesheet" href="{{asset("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css")}}">
<style>

</style>
@endpush
