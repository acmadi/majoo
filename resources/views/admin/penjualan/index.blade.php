@extends('layouts.app-lte')
@section('content')
    <div class="container">
        <div class="card mb-lg-32pt">
            <div class="card-body">

                {{-- 
                <div class="row">
                    <div class="col-12 pb-3">
                        <div id="components-app" class="demo">
                            <ol>
                                <todo-item
                                v-for="item in groceryList"
                                v-bind:todo="item"
                                v-bind:key="item.id"
                                ></todo-item>
                            </ol>
                            <my-button >
                            reload
                            </my-button>
                        </div>
                    </div>
                </div> 
                --}}
                <div class="row " id="formTransaksiPenualan">
                    <div class="col-12 pb-3">
                        @include('admin.penjualan.form-penjualan')
                    </div>
                </div> 
                <div class="row">
                    <div class="col-6 pb-3">
                        <button class="btn btn-info advance-search">Advance Search</button>
                    </div>
                    <div class="col-6 pb-3 ">
                        {{-- <a href="{{url()->current().'/create'}}" type="button" class="float-right btn btn-primary">Buat Baru</a> --}}
                        <button type="button" id="toggleFormTransaksi" class="float-right btn btn-primary">Buat Transaksi </button>
                    </div>
                </div>
                <div class="row show-advance-search hide pb-3">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama">
                        </div>
                    </div>
                    <div class="col-12">
                        <button class="btn btn-success pull-right btn-filter">Filter</button>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table mb-0 thead-border-top-0 table-nowrap yajra-dt">
                        <thead>
                            <tr>
                                <th style="width:1%">#</th>
                                
                                <th style="width:1px">No</th>
                                
                                <th style="width:1px">Tgl</th>
                                
                                <th style="width:1px">Pelanggan</th>
                                
                                <th style="width:1px">keterangan</th>
                                
                                <th style="width:1px">Total</th>
                                
                                {{-- <th style="width:1px">users_id</th> --}}
                                
                                <th style="width:1px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
        <!-- /.row -->
    
        <!-- /.row -->
        </div>
@endsection

@section('modal')
@endsection

@push('js')
    <script>
        datatable()
        function datatable(){
            let param = {
                nama: $('input[name="nama"').val(),
            }
            let ajax_dt = {
                //urll : API_URL + '/admin/master/agama/get-datatablexxxxxxxxxxxxx',
                urll : "{{route('penjualan.data')}}",
                method : 'get',
                data : param
            }
            let column_dt = [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                { data: 'no_penjualan', name: 'no_penjualan'},
                { data: 'tgl_penjualan', name: 'tgl_penjualan'},
                { data: 'pelanggan.nama_pelanggan', name: 'mt_pelanggan_id'},
                { data: 'keterangan', name: 'keterangan'},
                { data: 'total_bayar', name: 'total_bayar'},
                //{ data: 'users_id', name: 'users_id'},
                { data: 'aksi', orderable: false, searchable: false, className: 'text-center',
                    render: function(data, type, row, meta){
                        data = `
                            <button type="button" onclick="postDelete(this)" data-id="`+row.id+`" data-toggle="tooltip" title="Hapus" class="btn btn-sm btn-accent">
                                <i class="fa fa-trash"></i>
                            </button>`
                        //data = `
                            //<a href="`+ BASE_URL + '/admin/penjualan/edit/' + row.id+`" data-toggle="tooltip" title="Edit" class="btn btn-sm btn-primary">
                                //<i class="fa fa-pencil-alt" aria-hidden="true"></i>
                            //</a>
                            //<button type="button" onclick="postDelete(this)" data-id="`+row.id+`" data-toggle="tooltip" title="Hapus" class="btn btn-sm btn-accent">
                                //<i class="fa fa-trash"></i>
                            //</button>`
                        return data;
                        }

                    },
            ]
            initDataTables('.yajra-dt', ajax_dt, column_dt, false)
        }
        $('.btn-filter').on('click',function(){
            datatable()
        })

        function postDelete(e){
            let data = {
                id : $(e).data('id'),
            }
            let urll = "{{route('penjualan.index')}}/delete/"+data.id
            Swal.fire({
            title: 'Apakah anda yakin hapus data ini?',
            text: "Data yang dihapus tidak bisa dikembalikan",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus'
            }).then((result) => {
                if (result.isConfirmed) {
                    let response = customAjax('post', urll, data)
                    response = JSON.parse(response)
                    callSwall(response)
                    datatable()
                }
            })
        }


        $('.advance-search').on('click', function(){

            if($('.show-advance-search').hasClass('hide')){
                $('.show-advance-search').removeClass('hide');
            }else{
                $('.show-advance-search').addClass('hide')
            }

        })
        window.objTable = datatable
        $('#toggleFormTransaksi').on('click', function(e){
            e.preventDefault();
            if($('#formTransaksiPenualan').hasClass('hide')){
                $('#formTransaksiPenualan').removeClass('hide');
            }else{
                $('#formTransaksiPenualan').addClass('hide')
            }
        })
        $('#formTransaksiPenualan').addClass('hide')

        //=================================================================
        let totalBayar = 0;
        $('#addItem').on('click', function(e){
            e.preventDefault();
            let param  = {
                mt_produk_id: $('#item_mt_produk_id').val(),
                jumlah: $('#item_jumlah').val(),
                diskon: $('#item_diskon').val()
            }
            let response = customAjax('post', '{{route('penjualan.add-carts')}}', param)
            response = JSON.parse(response)
            console.log(response)
            $('#item_mt_produk_id').val(0)
            $('#item_jumlah').val(0)
            $('#item_diskon').val(0)
            callSwall(response)
            tableCart()
        })
        let objTableCart 
        function tableCart(){
            let param = {
                nama: $('input[name="nama"').val(),
            }
            let ajax_dt = {
                urll : "{{route('penjualan.my-carts')}}",
                method : 'get',
                data : param
            }
            let column_dt = [
                //{ data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                { data: 'kode_barang', name: 'kode_barang'},
                { data: 'nama_barang', name: 'nama_barang'},
                { data: 'harga_jual', name: 'harga_jual',
                     render: function(data, type, row, meta){
                        return formatRupiah(row.harga_jual, 'Rp. ')    
                    }
                },
                { data: 'jumlah', name: 'jumlah' },
                { data: 'diskon', name: 'diskon',
                     render: function(data, type, row, meta){
                        return `${row.diskon}%`
                    }
                },
                { data: 'subtotal', name: 'subtotal',
                    render: function(data, type, row, meta){
                        return formatRupiah(row.subtotal, 'Rp. ')    
                    }
                },
                { data: 'aksi', orderable: false, searchable: false, className: 'text-center',
                    render: function(data, type, row, meta){
                        data = `
                            <button type="button" onclick="editCart(this)" data-id="`+row.id+`" data-toggle="tooltip" title="Hapus" class="btn btn-sm btn-primary btn-accent">
                                <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                            </button>
                            <button type="button" onclick="deleteCart(this)" data-id="`+row.id+`" data-toggle="tooltip" title="Hapus" class="btn btn-sm btn-accent">
                                <i class="fa fa-trash"></i>
                            </button>`
                        return data;
                        }

                    },
            ]
            objTableCart = initDataTablesSources('#cart-table', ajax_dt, column_dt, false)
            //let data = customAjax('get', "{{route('penjualan.my-carts')}}", {})
            objTableCart.on( 'xhr', function () {
                var json = objTableCart.ajax.json();
                totalBayar = _.sumBy(json.data, 'subtotal')
                $('#total_bayar').val(formatRupiah(totalBayar, 'Rp. '))
            } );
        
        }
        tableCart()
        $('#kosongkanKeranjang').on('click', function(e){
            e.preventDefault()
            let response = customAjax('post', "{{route('penjualan.my-carts')}}/delete/all", {})
            response = JSON.parse(response)
            callSwall(response)
            tableCart()
        })
        function deleteCart(e){
            let data = {
                id : $(e).data('id'),
            }
            let urll = "{{route('penjualan.my-carts')}}/delete/"+data.id
            Swal.fire({
            title: 'Apakah anda yakin hapus data ini?',
            text: "Data yang dihapus tidak bisa dikembalikan",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus'
            }).then((result) => {
                if (result.isConfirmed) {
                    let response = customAjax('post', urll, data)
                    response = JSON.parse(response)
                    callSwall(response)
                    tableCart()
                }
            })
        }
        
        function editCart(e){
            let data = {
                id : $(e).data('id'),
            }
            let urll = "{{route('penjualan.my-carts')}}/edit/"+data.id
            let response = customAjax('get', urll, data)
            response = JSON.parse(response)
            console.log(response)
            if(response.status=='error'){
                callSwall(response)
            }
            $('#item_mt_produk_id').val(response.cart.mt_produk_id)
            $('#item_jumlah').val(response.cart.jumlah)
            $('#item_diskon').val(response.cart.diskon)
        }
        $('#money_in').on('change', function(e){
            e.preventDefault()
            let money_in = $(e.target).val()
            $('#money_out').val(formatRupiah(Number(money_in)-Number(totalBayar), 'Rp. '))
        })
        $('#money_out').val(formatRupiah(0, 'Rp. '))
        //$( '#money_in' ).mask('000.000.000', {reverse: true});
        $('#saveTransaction').on('click', function(e){
            e.preventDefault()
            const params = {
                no_penjualan: $("#no_penjualan").val(),
                tgl_penjualan: $("#tgl_penjualan").val(),
                mt_pelanggan_id: $("#mt_pelanggan_id").val(),
                keterangan: $("#keterangan").val(),
            }
            console.log(params)
            console.log($("#tgl_penjualan").val())
            let response = customAjax('post', '{{route("penjualan.checkout-carts")}}', params)
            response = JSON.parse(response)
            $('#money_out').val(formatRupiah(0, 'Rp. '))
            $('#money_in').val(0)
            callSwall(response)
            tableCart()
            datatable()
        })
        
    </script>
    <script>
        
    </script>
@endpush
@push('css')
    <style>
    .hide{
        display:none
    }
    </style>
@endpush