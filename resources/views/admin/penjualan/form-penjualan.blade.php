<div class="card">
    <div class="card-header">
        <h5 class="card-title m-0">Form Transaksi Penjualan</h5>
    </div>
    <div class="card-body">
        {{-- <h6 class="card-title">Special title treatment</h6> --}}

        <div class="row">
            <div class="col-lg-6">
                <table class="table ">
                  <tbody>
                    <tr>
                      <td style="width:30%">No Transaksi</td>
                      <td>
                        <input type="text" readonly placeholder="auto generate" required name="no_penjualan" id="no_penjualan" class="form-control form-control-sm" value="{{$data->no_penjualan ?? old('no_penjualan')}}">
                      </td>
                    </tr>
                    <tr>
                      <td style="width:30%">Tanggal Transaksi</td>
                      <td>
                        <input type="text" required name="tgl_penjualan" id="tgl_penjualan" class="form-control form-control-sm" value="{{$data->tgl_penjualan ?? old('tgl_penjualan')}}">
                      </td>
                    </tr>
                    <tr>
                      <td style="width:30%">Pelanggan</td>
                      <td>
                        {{-- <input type="text" required name="mt_pelanggan_id" id="mt_pelanggan_id" class="form-control form-control-sm" value="{{$data->mt_pelanggan_id ?? old('mt_pelanggan_id')}}"> --}}
                        <select class="form-control form-control-sm js-example-basic-single" style="width: 100%" name="mt_pelanggan_id" id="mt_pelanggan_id">
                            @foreach ($pelanggans as $key => $name)
                            <option {{($data->mt_pelanggan_id ?? old('mt_pelanggan_id'))==$key?'selected':''}} value="{{$key}}">{{$name}}</option>
                            @endforeach
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td style="width:30%">Keterangan</td>
                      <td>
                        <input type="text" required name="keterangan" id="keterangan" class="form-control form-control-sm" value="{{$data->keterangan ?? old('keterangan')}}">

                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-lg-6">
                <table class="table ">
                  <tbody>
                    <tr>
                      <td style="width:30%">Kode Barang</td>
                      <td>
                         <select class="form-control form-control-sm js-example-basic-single" style="width: 100%" name="mt_produk_id" id="item_mt_produk_id"> 
                            @foreach ($produks as $key => $name)
                            <option {{($data->mt_produk_id ?? old('mt_produk_id'))==$key?'selected':''}} value="{{$key}}">{{$name}}</option>
                            @endforeach
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td style="width:30%">Jumlah</td>
                      <td>
                        <input type="number" required name="jumlah" id="item_jumlah"  class="form-control form-control-sm" value="{{$data->jumlah ?? old('jumlah')}}">
                      </td>
                    </tr>
                    <tr>
                      <td style="width:30%">Diskon</td>
                      <td>
                        <input type="number" required name="diskon" id="item_diskon"  class="form-control form-control-sm" value="{{$data->diskon ?? old('diskon')}}">
                      </td>
                    </tr>
                    <tr >
                      <td class="bg-red-500 break-all " colspan="2">
                        <button class="btn btn-info float-right" id="addItem">Tambah Barang</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            Keranjang
                            <button id="kosongkanKeranjang"> Kosongkan Keranjang
                            </button>
                        </h5>
                        <div class="table-responsive">
                            <table class="table mb-0 thead-border-top-0 table-nowrap" id="cart-table">
                                <thead>
                                    <tr>
                                        <th style="width:1px">Kode</th>
                                        
                                        <th style="width:1px">Nama Barang</th>
                                        
                                        <th style="width:1px">Harga</th>
                                        <th style="width:1px">Jumlah</th>
                                        <th style="width:1px">Diskon</th>
                                        <th style="width:1px">Subtotal</th>
                                        
                                        <th style="width:1px;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="cart-table-body">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- /.card -->
            </div>
            <!-- /.col-md-6 -->
            
            <!-- /.col-md-6 -->
        </div>
        <div class="row">
            
                <div class="col-lg-6">
                </div>
                <div class="col-lg-6">
                    <table class="table ">
                    <tbody>
                        <tr>
                            <td style="width:30%">GRAND TOTAL BELANJA (Rp.)</td>
                            <td>
                                <input type="text" required id="total_bayar" readonly name="total_bayar" class="form-control form-control-sm" value="{{$data->total_bayar ?? old('total_bayar')}}">
                            </td>
                        </tr>
                        <tr>
                            <td style="width:30%">UANG BAYAR (Rp.)</td>
                            <td>
                                <input type="text" required id="money_in" name="money_in" class="form-control form-control-sm" value="{{$data->money_in ?? old('money_in')}}">
                            </td>
                        </tr>
                        <tr>
                            <td style="width:30%">UANG KEMBALI (Rp.)</td>
                            <td>
                                <input type="text" required id="money_out" name="money_out" readonly class="form-control form-control-sm" value="{{$data->money_out ?? old('money_out')}}">

                            </td>
                        </tr>
                        <tr >
                        <td class="bg-red-500 break-all " colspan="2">
                            <button class="btn btn-info float-right" id="saveTransaction">Simpan Transaksi</button>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                {{-- <button class="btn btn-info float-right" id="addItem">Simpan Transaksi</button> --}}
            
        </div>
    </div>
</div>