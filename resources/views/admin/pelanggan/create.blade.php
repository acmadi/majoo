@extends('layouts.app-lte')
@section('content')
    <div class="container">
        <div class="card mb-lg-32pt">
            <div class="card-body">
                <div class="col">
                    {!!parseError() !!}
                    <form class="formAdd" method="post" action="{{$action}}">
                        @csrf
                        {{-- <input type="hidden" name="_method" value="{{$_method}}"> --}}
                        <div class="col-lg-3  col-sm-12">
                            
                            <div class="form-group">
                                <label>Nama Pelanggan</label>
                                <input type="text" required name="nama_pelanggan" class="form-control" value="{{$data->nama_pelanggan ?? old('nama_pelanggan')}}">
                            </div>
                            <div class="form-group">
                                <label>Nama Toko</label>
                                <input type="text" required name="nama_toko" class="form-control" value="{{$data->nama_toko ?? old('nama_toko')}}">
                            </div>
                            
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" required name="alamat" class="form-control" value="{{$data->alamat ?? old('alamat')}}">
                            </div>
                            
                            <div class="form-group">
                                <label>Telp</label>
                                <input type="text" required name="telp" class="form-control" value="{{$data->telp ?? old('telp')}}">
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary btn-add-banner">Submit</button>
                        </div>
                    </form>
                </div>
            
            </div>
        </div>
    
        <!-- /.row -->
    
        <!-- /.row -->
        </div>
@endsection

@section('modal')
@endsection

@push('js')
    
@endpush
@push('css')
@endpush