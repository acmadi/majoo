<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <title>{{ config('app.name', 'Laravel') }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @include('layouts.app-lte.styles')

</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="{{asset("/")}}" class="navbar-brand">
        <img src="{{asset("dist/img/AdminLTELogo.png")}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
  <!-- Messages Dropdown Menu -->
  
    @if (Route::has('login') )
            @auth
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="{{ url('/dashboard') }}" role="button">
                    <i class="fas fa-th-large"></i>
                    Dashboard
                    </a>
                </li>
                <li class="nav-item">
                  <form method="POST" action="{{ route('logout') }}">
                      @csrf
                      <a class="nav-link" href="{{route('logout')}}" role="button" class="bg-blue-300" onclick="event.preventDefault();
                                          this.closest('form').submit();">
                        <i class="fas fa-lock mr-2"></i>
                        {{-- {{ Auth::user()->name }} --}}
                        Logout
                      </a>
                  </form>
                  
                </li>
                
            @else
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="{{ route('login') }}" role="button">
                    <i class="fas fa-th-large"></i>
                    Login
                    </a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="{{ route('register') }}" role="button">
                    <i class="fas fa-th-large"></i>
                    Register
                    </a>
                </li>
                @endif
            @endauth
        
    @endif
  
</ul>
      
    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            {{-- <h1 class="m-0"> Top Navigation <small>Example 3.0</small></h1> --}}
            <h1 class="m-0"> {{$title??'Selamat datang di aplikasi POS '}}{{ config('app.name', 'Laravel') }}</h1>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      @yield('content')
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  
</div>
<!-- ./wrapper -->

  
</body>
</html>
