$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/* 
$('.js-datetimepicker').flatpickr({
    enableTime: true,
    dateFormat: "Y-m-d H:i",
});

$('.js-datepicker').flatpickr({
    enableTime: true,
    dateFormat: "Y-m-d",
});
 */
function customAjax(method, urll, data = null) {
    let response = $.ajax({
        type: method,
        url: urll,
        data: data,
        dataType: "json",
        async: false,
    });

    return response.responseText
}

function callSwall(response) {

    let text = ''
    let url = response.url != undefined ? response.url : ''

    if (!response.status || response.status == 'error') {
        $.each(response.errors, function(key, value) {
            text += value + `<br>`
        });
    }

    return Swal.fire({
        title: response.message,
        html: text,
        icon: response.status,
        timer: 5000
    }).then(() => {
        if (url != '') {
            window.location.href = url
        }
    })

}

function searchEmail(element) {
    let urll = BASE_URL + '/search-email'
    let email = $(element).val()
    let data = {
        email: email
    }
    let response = customAjax('post', urll, data)
    response = JSON.parse(response)
        // console.log(response.status)
    if (response.status == 'error') {
        $('.email-error').show()
    } else {
        $('.email-error').hide()
    }
}

function initDataTables(element, ajax, column, search = true) {

    $(element).DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        searching: search,
        ajax: {
            url: ajax.urll,
            method: ajax.method,
            data: ajax.data
        },
        columns: column
    });
}
function initDataTablesSources(element, ajax, column, search = true) {

    return $(element).DataTable({
        // processing: true,
        // serverSide: false,
        destroy: true,
        // searching: search,
        ajax: {
            url: ajax.urll,
            method: ajax.method,
            data: ajax.data
        },
        columns: column,
        "paging":   false,
        "ordering": false,
        "info":     false
    });
}
function coba(){
    alert('coba')
}

function formatRupiah(angka, prefix){
    // var number_string = angka.replace(/[^,\d]/g, '').toString(),
    var number_string = angka.toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function htmlDatatable(id, config ) {
    config = config || {}
    $(id).DataTable(config);
}

function btnDelete(type, urll, data) {

    Swal.fire({
        title: 'Apakah anda yakin hapus data ini?',
        text: "Data yang dihapus tidak bisa dikembalikan",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus'
    }).then((result) => {
        if (result.isConfirmed) {
            let response = customAjax(type, urll, data)
            response = JSON.parse(response)
            callSwall(response)
        }
    })
}

function showChangeImage(input, id_element) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $(id_element).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}

function changeIframe(input, id_element, tipe){
    var vall = $(input).val()

    if(tipe == 'gambar'){
        return showChangeImage(input, id_element)
    }else if(vall == 'file_sharing'){
        return $(id_element).attr('src', vall);
    }
}

function logout() {
    Swal.fire({
        title: 'Apakah anda yakin untuk keluar ?',
        text: "Konfirmasi Keluar",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Logout'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = BASE_URL + '/logout'
        }
    })
}

function swalLoading(allow_click = true) {
    Swal.fire({
        icon: 'warning',
        title: 'Mohon menunggu',
        html: 'Sedang memproses data',
        didOpen: () => {
            Swal.showLoading()
        },
        allowOutsideClick: allow_click
    })
}

function submitForm(form_id_element) {

    Swal.fire({
        title: 'Yakin menambahkan data?',
        text: "Konfirmasi Menambahkan Data",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
    }).then((result) => {
        if (result.isConfirmed) {
            swalLoading(false)
            $("#" + form_id_element).submit()
        }
    })
}


function listFileSharing(cls){
    $(cls).select2({
        ajax: {
            url: `${API_URL}/admin/file-sharing`,
            data: function (params) {
                var query = {
                    search: params.term,
                }
                return query;
            },
            processResults: function (data, params) {
                 console.log(data.data, params)
                return {
                    results: data.data,
                };
            },
            cache: true
        }
    });
}
function cetakElement(DivID) {
    var disp_setting="toolbar=yes,location=no,";
    disp_setting+="directories=yes,menubar=yes,";
    disp_setting+="scrollbars=yes,width=650, height=600, left=100, top=25";
    var content_vlue = document.getElementById(DivID).innerHTML;
    var docprint=window.open("","",disp_setting);
    docprint.document.open();
    docprint.document.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"');
    docprint.document.write('"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
    docprint.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">');
    docprint.document.write('<head><title>My Title</title>');
    docprint.document.write('<style type="text/css">body{ margin:0px;');
    docprint.document.write('font-family:verdana,Arial;color:#000;');
    docprint.document.write('font-family:Verdana, Geneva, sans-serif; font-size:10px;}');
    docprint.document.write('a{color:#000;text-decoration:none;} </style>');
    docprint.document.write('</head><body onLoad="self.print()"><center>');
    docprint.document.write(content_vlue);
    docprint.document.write('</center></body></html>');
    docprint.document.close();
    docprint.focus();
}
    